package com.diamonddev.payup.menu;

import org.cogaen.core.Core;
import org.cogaen.logging.LoggingService;
import org.cogaen.name.CogaenId;
import org.cogaen.resource.ResourceService;
import org.cogaen.state.BasicState;
import org.cogaen.view.View;


public class MenuState extends BasicState{
	
	public static final CogaenId ID = new CogaenId("Menu");
	public static final CogaenId END_OF_STATE = new CogaenId("EndOfMenu");
	
	private View view;


	public MenuState(Core core) {
		super(core);
		
		this.view = new MenuView(core);
		
		ResourceService resSrv = ResourceService.getInstance(core);
		resSrv.createGroup(ID);
		this.view.registerResources(ID);
	}

	@Override
	public void onEnter() {
		super.onEnter();
		this.view.engage();
		ResourceService.getInstance(getCore()).unloadGroup(ID);
		LoggingService.getInstance(getCore()).logDebug("Game", "MenuState entered");
	}
	
	@Override
	public void onExit() {
		super.onExit();
		ResourceService.getInstance(getCore()).unloadGroup(ID);
		LoggingService.getInstance(getCore()).logDebug("Game", "MenuState left");
		this.view.disengage();
	}
}
