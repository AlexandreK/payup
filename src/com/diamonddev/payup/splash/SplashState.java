package com.diamonddev.payup.splash;

import org.cogaen.core.Core;
import org.cogaen.event.EventService;
import org.cogaen.event.SimpleEvent;
import org.cogaen.logging.LoggingService;
import org.cogaen.name.CogaenId;
import org.cogaen.resource.ResourceService;
import org.cogaen.state.BasicState;
import org.cogaen.view.View;

public class SplashState extends BasicState{

	//ID of the State and of the State'shape end
	public static final CogaenId ID = new CogaenId("SplashState");
	public static final CogaenId END_OF_STATE = new CogaenId("EndOfSplash");
	
	//Time in seconds the SplashState should be Visible
	private static final double DISPLAY_TIME = 1;
	
	private View view;

	public SplashState(Core core) {
		super(core);
		this.view = new SplashView(core);
		
		ResourceService resSrv = ResourceService.getInstance(core);
		resSrv.createGroup(ID);
		this.view.registerResources(ID);
	}
	
	@Override
	public void onEnter() {
		this.view.engage();
		ResourceService.getInstance(getCore()).loadGroup(ID);
		
		EventService.getInstance(getCore()).dispatchEvent(new SimpleEvent(END_OF_STATE),DISPLAY_TIME);
		LoggingService.getInstance(getCore()).logDebug("Game", "splash state entered");
	}

	@Override
	public void onExit() {
		this.view.disengage();
		ResourceService.getInstance(getCore()).unloadGroup(ID);
		LoggingService.getInstance(getCore()).logDebug("Game", "splash state left");
	}

}
