package com.diamonddev.payup;

import java.io.FileNotFoundException;

import org.cogaen.box2d.PhysicsService;
import org.cogaen.core.Core;
import org.cogaen.core.ServiceException;
import org.cogaen.entity.EntityService;
import org.cogaen.event.Event;
import org.cogaen.event.EventListener;
import org.cogaen.event.EventService;
import org.cogaen.logging.ConsoleLogger;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.input.KeyboardService;
import org.cogaen.lwjgl.scene.ForceVisualizerService;
import org.cogaen.lwjgl.scene.SceneService;
import org.cogaen.name.IdService;
import org.cogaen.property.PropertyService;
import org.cogaen.resource.ResourceService;
import org.cogaen.state.GameStateService;
import org.cogaen.task.TaskService;
import org.cogaen.time.Clock;
import org.cogaen.time.TimeService;

import com.diamonddev.payup.game.PlayState;
import com.diamonddev.payup.game.service.CollisionService;
import com.diamonddev.payup.game.service.GameTimeService;
import com.diamonddev.payup.game.service.LevelService;
import com.diamonddev.payup.game.service.MapService;
import com.diamonddev.payup.game.util.Log;
import com.diamonddev.payup.splash.SplashState;

public class PayUp implements EventListener{

	private Core core;

	public static void main(String[] args) throws FileNotFoundException, Exception {
		PayUp game = new PayUp();
		game.run();
		
		
	}

	public PayUp() {
		this.core = new Core();

	}

	public void run() throws ServiceException {
		initGame();
		initStates();
		gameLoop();
	}

	private void initStates() {
		EventService evSrv = EventService.getInstance(this.core);
		evSrv.addListener(this, SceneService.WINDOW_CLOSE_REQUEST);
		
		ForceVisualizerService.getInstance(core).setForceScale(0.01);
		SceneService.getInstance(core).setVSync(true);
		GameStateService stateSrv = GameStateService.getInstance(this.core);
		stateSrv.addState(new SplashState(core), SplashState.ID);
		stateSrv.addState(new PlayState(core),PlayState.ID);
		
		stateSrv.addTransition(SplashState.ID, PlayState.ID, SplashState.END_OF_STATE);
		stateSrv.addTransition(PlayState.ID, SplashState.ID, PlayState.GAME_OVER);
		stateSrv.setCurrentState(SplashState.ID);
		
	}

	private void gameLoop() {
		Clock clock = new Clock();
		SceneService scnSrv = SceneService.getInstance(this.core);
		GameStateService stateSrv = GameStateService.getInstance(this.core);

		while (!stateSrv.isEndState()) {
			clock.tick();
			this.core.update(clock.getDelta());
			scnSrv.renderScene();
		}
		
		this.core.shutdown();	

	}

	private void initGame() throws ServiceException {
		this.core.addService(new ConsoleLogger());
		this.core.addService(new IdService());
		this.core.addService(new TimeService());
		this.core.addService(new PropertyService("mygame.cfg"));
		this.core.addService(new EventService());
		this.core.addService(new GameStateService());
		this.core.addService(new ResourceService());
		this.core.addService(new SceneService(1024, 768, false, true));
		this.core.addService(new KeyboardService());
		this.core.addService(new EntityService());
		this.core.addService(new CollisionService());
		this.core.addService(new TaskService());
		this.core.addService(new MapService());
		this.core.addService(new PhysicsService());
		this.core.addService(new ForceVisualizerService());
		this.core.addService(new GameTimeService());
		this.core.addService(new LevelService());
		this.core.startup();
		Log.init(LoggingService.getInstance(this.core));

	}

	@Override
	public void handleEvent(Event event) {
		 if(event.isOfType(SceneService.WINDOW_CLOSE_REQUEST)) {
				GameStateService.getInstance(this.core).setCurrentState(GameStateService.END_STATE_ID);
			}
		
	}

}
