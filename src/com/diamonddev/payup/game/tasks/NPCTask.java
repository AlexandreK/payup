package com.diamonddev.payup.game.tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.cogaen.core.Core;
import org.cogaen.entity.EntityService;
import org.cogaen.event.Event;
import org.cogaen.event.EventListener;
import org.cogaen.event.EventService;
import org.cogaen.logging.LoggingService;
import org.cogaen.task.AbstractTask;
import org.cogaen.time.TimeService;
import org.cogaen.time.Timer;

import com.diamonddev.payup.game.entity.HumanEntity;
import com.diamonddev.payup.game.entity.humans.PedestrianEntity;
import com.diamonddev.payup.game.event.DestroyEvent;
import com.diamonddev.payup.game.util.Log;

public class NPCTask extends AbstractTask implements EventListener {

	private static final int MAX_NPCS = 100;
	private Timer t;
	private Random rdm;

	private int livingNPCs= 0;
	private double nextNPCUpdate = 500;
	private List<HumanEntity> npcs = new ArrayList<HumanEntity>();
	
	public NPCTask(Core core) {
		super(core);
		t = TimeService.getInstance(core).getTimer();
		rdm = new Random();
		calculateNextNPCUpdate();
		
		EventService.getInstance(core).addListener(this, DestroyEvent.TYPE_ID);
	}
	
	
	@Override
	public void update() {
		if(t.getTime()>nextNPCUpdate){
			calculateNextNPCUpdate();
			checkNPCs();
		}
	}

	private void checkNPCs() {
		if(livingNPCs < MAX_NPCS){
			HumanEntity h = new PedestrianEntity(getCore(),rdm.nextDouble()*20,rdm.nextDouble()*20);
			EntityService.getInstance(getCore()).addEntity(h);
			npcs.add(h);
			livingNPCs++;
		}
		
	}


	private void calculateNextNPCUpdate() {
		double timeToNextUpdate = rdm.nextDouble()*0.6;
		nextNPCUpdate = t.getTime() + timeToNextUpdate;
	}


	@Override
	public void destroy() {
	}


	@Override
	public void handleEvent(Event event) {
		if(event.getTypeId().equals(DestroyEvent.TYPE_ID)){
			handleDestroyEvent((DestroyEvent)event);
		}
	}


	private void handleDestroyEvent(DestroyEvent event) {
		if(event.getEntityTypeId().equals(PedestrianEntity.TYPE_ID)){
			livingNPCs--;
		}
		
	}

}
