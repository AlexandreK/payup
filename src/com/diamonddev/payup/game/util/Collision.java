package com.diamonddev.payup.game.util;

public class Collision {

	private boolean collideNorth,collideSouth,collideEast,collideWest = false;

	public boolean isCollideNorth() {
		return collideNorth;
	}

	public void setCollideNorth(boolean collideNorth) {
		this.collideNorth = collideNorth;
	}

	public boolean isCollideEast() {
		return collideEast;
	}

	public void setCollideEast(boolean collideEast) {
		this.collideEast = collideEast;
	}

	public boolean isCollideWest() {
		return collideWest;
	}

	public void setCollideWest(boolean collideWest) {
		this.collideWest = collideWest;
	}

	public boolean isCollideSouth() {
		return collideSouth;
	}

	public void setCollideSouth(boolean collideSouth) {
		this.collideSouth = collideSouth;
	}
	
	public String toString(){
		return "North:" + isCollideNorth() + " South:" + isCollideSouth() + " East:" + isCollideEast() + " West:" + isCollideWest();
	}
	public void reset(){
		collideEast = false;
		collideWest = false;
		collideNorth = false;
		collideSouth = false;
	}
}
