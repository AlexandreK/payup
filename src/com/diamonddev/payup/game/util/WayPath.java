package com.diamonddev.payup.game.util;

import java.util.ArrayList;
import java.util.List;

public class WayPath {
	
	public static final int NORTH = 1;
	public static final int SOUTH = 2;
	public static final int EAST = 3;
	public static final int WEST = 4;
	
	public List<Integer> nextStep = new ArrayList<Integer>();
	private int actualStep = 0;
	
	public WayPath(){
		
	}
	
	public void goNorth(){
		nextStep.add(NORTH);
	}
	public void goSouth(){
		nextStep.add(SOUTH);
	}
	public void goEast(){
		nextStep.add(EAST);
	}
	public void goWest(){
		nextStep.add(WEST);
	}
	
	public int nextStep(){
		return nextStep.get(actualStep++);
	}
	
	

}
