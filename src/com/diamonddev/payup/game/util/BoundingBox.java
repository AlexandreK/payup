package com.diamonddev.payup.game.util;

import org.cogaen.math.Vector2;

public class BoundingBox {
	private double width, height, minX,minY,maxX,maxY = 0;
	
	public BoundingBox(double x, double y, double radius){
		this.width = radius*2+0.1;
		this.height = radius*2+0.1;
		updateBox(x,y);
	}
	
	public BoundingBox(double x, double y, double width, double height){
		this.width = width+0.1;
		this.height = height+0.1;
		updateBox(x,y);
	}
	public void updateBox(double x, double y){
		minX = x-width/2;
		minY = y-height/2;
		maxX = x + width/2;
		maxY = y + height/2;
	}
	public boolean collidesWith(BoundingBox b){
		return  (minX < b.getMaxX()) && (maxX > b.getMinX()) &&
		        (minY < b.getMaxY()) && (maxY > b.getMinY());
		        
	}

	public double getMinX() {
		return minX;
	}

	public double getMinY() {
		return minY;
	}

	public double getMaxX() {
		return maxX;
	}

	public double getMaxY() {
		return maxY;
	}
	
//	public double getPosX(){
//		return minX - (maxX-minX)/2;
//	}
//	
//	public double getPosY(){
//		return minY - (maxY-minY)/2;
//	}

	public Collision getCollisionDirection(BoundingBox b) {
		if(collidesWith(b)){
			Collision c = new Collision();
			if(getMinX()<b.getMaxX()&&getMaxX()>b.getMaxX()&&getMinX()>b.getMinX()){
				c.setCollideWest(true);
			}
			if(getMaxX()>b.getMinX() && getMaxX()<b.getMaxX()&&getMinX()<b.getMinX()){
				c.setCollideEast(true);
			}
			if(getMinY()<b.getMaxY()&&getMaxY()>b.getMaxY()&&getMinY()>b.getMinY()){
				c.setCollideSouth(true);
			}
			if(getMaxY()>b.getMinY() && getMaxY()<b.getMaxY()&&getMinY()<b.getMinY()){
				c.setCollideNorth(true);
			}
			
			return c;
		} else {
			return null;
		}
	}
	
}
