package com.diamonddev.payup.game.util;

import org.cogaen.math.Vector2;

public class VectorTool {
	
	public static Vector2 turn(Vector2 vec, double winkel){
		double cos = Math.cos(winkel);
		double sin = Math.sin(winkel);
		double x = (cos*vec.x)*((-sin)*vec.y);
		double y = (sin*vec.x)*(cos*vec.y);
		return new Vector2(x,y);
	}

}
