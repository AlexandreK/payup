package com.diamonddev.payup.game.util;

import java.awt.Color;

import org.cogaen.core.Core;
import org.cogaen.entity.Entity;
import org.cogaen.logging.LoggingService;

import com.diamonddev.payup.game.entity.MapElement;
import com.diamonddev.payup.game.entity.buildings.Building2Entity;
import com.diamonddev.payup.game.entity.buildings.Building3Entity;
import com.diamonddev.payup.game.entity.buildings.Building4Entity;
import com.diamonddev.payup.game.entity.buildings.BuildingHospitalEntity;
import com.diamonddev.payup.game.entity.buildings.BuildingPoliceEntity;
import com.diamonddev.payup.game.entity.buildings.DefaultBuildingEntity;
import com.diamonddev.payup.game.entity.buildings.GarageEntity;
import com.diamonddev.payup.game.entity.textures.QuayEntity;
import com.diamonddev.payup.game.entity.textures.RoadEntity;
import com.diamonddev.payup.game.entity.textures.SandEntity;
import com.diamonddev.payup.game.entity.textures.SidewalkEntity;
import com.diamonddev.payup.game.entity.textures.WaterEntity;

public class MapGenerator {
	private static final String ENTITY_ROAD = "ffffff";
	private static final String ENTITY_SIDEWALK = "aaaaaa";
	private static final String ENTITY_BUILDING_1 = "000";
	private static final String ENTITY_BUILDING_2 = "4f4f4f";
	private static final String ENTITY_BUILDING_3 = "2b1d1d";
	private static final String ENTITY_BUILDING_4 = "785621";
	
	private static final String ENTITY_BUILDING_POLICE = "6b8cf6";
	private static final String ENTITY_BUILDING_HOSPITAL = "f66b6b";
	
	private static final String ENTITY_GARAGE = "668262";
	private static final String ENTITY_WATER = "00ff";
	private static final String ENTITY_QUAI = "551155";
	private static final String ENTITY_SAND = "ffff0";
	private static final String ENTITY_CURB = "555555";
	
	private String colorCode;
	Core core;
	public MapGenerator(Core core){
		this.core = core;
	}
	public void setColor(int color){
		Color c = new Color(color);
		colorCode = Integer.toHexString(c.getRed()) + "" + Integer.toHexString(c.getGreen())+""+Integer.toHexString(c.getBlue());
	}
	public MapElement getEnitity(double x, double y){
		if(colorCode.equals(ENTITY_ROAD)){
			return new RoadEntity(getCore(),x,y);
		}
		if(colorCode.equals(ENTITY_SIDEWALK)){
			return new SidewalkEntity(getCore(),x,y);
		}
		if(colorCode.equals(ENTITY_BUILDING_1)){
			return new DefaultBuildingEntity(getCore(),x,y);
		}
		if(colorCode.equals(ENTITY_BUILDING_2)){
			return new Building2Entity(getCore(),x,y);
		}
		if(colorCode.equals(ENTITY_BUILDING_3)){
			return new Building3Entity(getCore(),x,y);
		}
		if(colorCode.equals(ENTITY_BUILDING_4)){
			return new Building4Entity(getCore(),x,y);
		}
		if(colorCode.equals(ENTITY_BUILDING_HOSPITAL)){
			return new BuildingHospitalEntity(getCore(),x,y);
		}
		if(colorCode.equals(ENTITY_BUILDING_POLICE)){
			return new BuildingPoliceEntity(getCore(),x,y);
		}
		if(colorCode.equals(ENTITY_GARAGE)){
			return new GarageEntity(getCore(),x,y);
		}
		if(colorCode.equals(ENTITY_WATER)){
			return new WaterEntity(getCore(),x,y);
		}
		if(colorCode.equals(ENTITY_SAND)){
			return new SandEntity(getCore(),x,y);
		}
		if(colorCode.equals(ENTITY_QUAI)){
			return new QuayEntity(getCore(),x,y);
		}
		
		
		LoggingService.getInstance(getCore()).logDebug("TEXT",colorCode);
		return new RoadEntity(getCore(),x,y);
		
	}

	private Core getCore() {
		return core;
	}

}
