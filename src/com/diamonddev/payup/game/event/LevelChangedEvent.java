package com.diamonddev.payup.game.event;

import org.cogaen.event.Event;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.util.Collision;

public class LevelChangedEvent extends Event {

	public static final CogaenId TYPE_ID = new CogaenId("CollisionEvent");
	
	private CogaenId EntityId;
	private Collision collision;

	public CogaenId getEntityId() {
		return EntityId;
	}

	public void setEntityId(CogaenId entityId) {
		EntityId = entityId;
	}

	public Collision getCollision() {
		return collision;
	}

	public void setCollision(Collision collision) {
		this.collision = collision;
	}

	public LevelChangedEvent(CogaenId entityId, Collision collision) {
		super();
		EntityId = entityId;
		this.collision = collision;
	}

	@Override
	public CogaenId getTypeId() {
		return TYPE_ID;
	}

}
