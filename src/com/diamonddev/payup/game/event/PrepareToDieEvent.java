package com.diamonddev.payup.game.event;

import org.cogaen.event.Event;
import org.cogaen.name.CogaenId;

public class PrepareToDieEvent extends Event {
	
	public static final CogaenId TYPE_ID = new CogaenId("PrepareToDieEvent");
	
	private CogaenId dieId;
	
	public PrepareToDieEvent(CogaenId dieId) {
		super();
		this.dieId = dieId; 
	}

	@Override
	public CogaenId getTypeId() {
		return TYPE_ID;
	}

	public CogaenId getDieId() {
		return dieId;
	}

}
