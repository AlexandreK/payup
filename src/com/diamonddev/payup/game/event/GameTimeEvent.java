package com.diamonddev.payup.game.event;

import org.cogaen.event.Event;
import org.cogaen.name.CogaenId;

public class GameTimeEvent extends Event{

	public static final CogaenId TYPE_ID = new CogaenId("GameTimeEvent");
	public int time;

	public GameTimeEvent(int time) {
		super();
		this.time = time;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	@Override
	public CogaenId getTypeId() {
		return TYPE_ID;
	}

}
