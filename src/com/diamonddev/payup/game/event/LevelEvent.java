package com.diamonddev.payup.game.event;

import org.cogaen.event.Event;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.logic.Level;

public class LevelEvent extends Event {

	public static final CogaenId TYPE_ID = new CogaenId("LevelEvent");
	private Level level;

	public LevelEvent(Level level) {
		super();
		this.level = level;
	}

	@Override
	public CogaenId getTypeId() {
		return TYPE_ID;
	}

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

}
