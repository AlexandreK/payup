package com.diamonddev.payup.game.event;

import org.cogaen.event.Event;
import org.cogaen.name.CogaenId;

public class MoneyEarnedEvent extends Event {
	
	public static final CogaenId TYPE_ID = new CogaenId("MoneyEarnedEvent");
	private double money;

	public MoneyEarnedEvent(double money) {
		super();
		this.money = money;
	}

	@Override
	public CogaenId getTypeId() {
		// TODO Auto-generated method stub
		return TYPE_ID;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

}
