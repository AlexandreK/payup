package com.diamonddev.payup.game.event;

import org.cogaen.entity.Entity;

import com.diamonddev.payup.game.entity.Collidable;
import com.diamonddev.payup.game.util.BoundingBox;


public interface CollisionListener {

	public void handleCollision(Collidable e);
	public BoundingBox getBoundingBox();
	
}
