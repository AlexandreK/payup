package com.diamonddev.payup.game.event;

import org.cogaen.event.Event;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.entity.CarEntity;

public class EnterCarEvent extends Event {

	public static final CogaenId TYPE_ID = new CogaenId("EnterCarEvent");
	private CarEntity car;

	public EnterCarEvent(CarEntity car){
		this.setCar(car);
	}
	@Override
	public CogaenId getTypeId() {
		return TYPE_ID;
	}
	public CarEntity getCar() {
		return car;
	}
	public void setCar(CarEntity car) {
		this.car = car;
	}

}
