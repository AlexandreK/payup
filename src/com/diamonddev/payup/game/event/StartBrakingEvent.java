package com.diamonddev.payup.game.event;

import org.cogaen.event.Event;
import org.cogaen.name.CogaenId;

public class StartBrakingEvent extends Event {

	public static final CogaenId TYPE_ID = new CogaenId("StartBrakingEvent");

	@Override
	public CogaenId getTypeId() {
		return TYPE_ID;
	}

}
