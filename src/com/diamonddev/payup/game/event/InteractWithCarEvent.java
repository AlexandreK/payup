package com.diamonddev.payup.game.event;

import org.cogaen.event.Event;
import org.cogaen.name.CogaenId;

public class InteractWithCarEvent extends Event {

	public static final CogaenId TYPE_ID = new CogaenId("InteractWithCarEvent");

	@Override
	public CogaenId getTypeId() {
		return TYPE_ID;
	}

}
