package com.diamonddev.payup.game.event;

import org.cogaen.event.Event;
import org.cogaen.name.CogaenId;

public class ExitCarEvent extends Event {

	public static final CogaenId TYPE_ID = new CogaenId("ExitCarEvent");

	@Override
	public CogaenId getTypeId() {
		return TYPE_ID;
	}

}
