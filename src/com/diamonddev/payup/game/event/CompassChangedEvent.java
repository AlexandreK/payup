package com.diamonddev.payup.game.event;

import org.cogaen.event.Event;
import org.cogaen.math.Vector2;
import org.cogaen.name.CogaenId;

public class CompassChangedEvent extends Event {

	public static final CogaenId TYPE_ID = new CogaenId("CompassChangesEvent");
	private double angle;
	Vector2 goal;
	private int distance;



	public CompassChangedEvent(double angle, Vector2 goal, int distance) {
		super();
		this.angle = angle;
		this.goal = goal;
		this.distance = distance;
	}

	public Vector2 getGoal() {
		return goal;
	}

	public void setGoal(Vector2 goal) {
		this.goal = goal;
	}

	@Override
	public CogaenId getTypeId() {
		return TYPE_ID;
	}

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

}
