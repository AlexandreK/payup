package com.diamonddev.payup.game.event;

import org.cogaen.event.Event;
import org.cogaen.name.CogaenId;

public class MapBoundsEvent extends Event {

	public static final CogaenId TYPE_ID = new CogaenId("MapBoundsEvent");
	private int x = 0;
	private int y = 0;
	public MapBoundsEvent(int x, int y) {
		this.setX(x);
		this.setY(y);
	}

	@Override
	public CogaenId getTypeId() {
		return TYPE_ID;
	}

	public int getX() {
		return x;
	}

	private void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	private void setY(int y) {
		this.y = y;
	}

}
