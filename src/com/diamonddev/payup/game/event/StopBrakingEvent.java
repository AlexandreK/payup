package com.diamonddev.payup.game.event;

import org.cogaen.event.Event;
import org.cogaen.name.CogaenId;

public class StopBrakingEvent extends Event {

	public static final CogaenId TYPE_ID = new CogaenId("StopBrakingEvent");

	@Override
	public CogaenId getTypeId() {
		return TYPE_ID;
	}

}
