package com.diamonddev.payup.game.event;

import org.cogaen.event.Event;
import org.cogaen.name.CogaenId;

public class GameOverEvent extends Event {

	public static final CogaenId TYPE_ID = new CogaenId("GameOverEvent");

	@Override
	public CogaenId getTypeId() {
		// TODO Auto-generated method stub
		return TYPE_ID;
	}

}
