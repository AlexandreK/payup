package com.diamonddev.payup.game.event;

import org.cogaen.box2d.Pose2D;
import org.cogaen.event.Event;
import org.cogaen.name.CogaenId;


public class SpawnEvent extends Event {

	public static final CogaenId TYPE_ID = new CogaenId("Spawn");

	private CogaenId entityId;
	private CogaenId entityTypeId;
	private double posX;
	private double posY;
	private double angle;
	
	public SpawnEvent(CogaenId entityId, CogaenId entityTypeId, Pose2D pose) {
		super();
		this.entityTypeId = entityTypeId;
		this.entityId = entityId;
		this.posX = pose.getPosX();
		this.posY = pose.getPosY();
		this.angle = pose.getAngle();
	}

	@Override
	public CogaenId getTypeId() {
		return TYPE_ID;
	}

	public CogaenId getEntityTypeId() {
		return entityTypeId;
	}

	public CogaenId getEntityId() {
		return entityId;
	}

	public double getPosX() {
		return posX;
	}

	public double getPosY() {
		return posY;
	}

	public double getAngle() {
		return angle;
	}
	
}
