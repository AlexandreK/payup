package com.diamonddev.payup.game;

import org.cogaen.box2d.PhysicsService;
import org.cogaen.box2d.Pose2D;
import org.cogaen.core.Core;
import org.cogaen.entity.EntityService;
import org.cogaen.event.Event;
import org.cogaen.event.EventListener;
import org.cogaen.event.EventService;
import org.cogaen.lwjgl.scene.SceneService;
import org.cogaen.name.CogaenId;
import org.cogaen.resource.ResourceService;
import org.cogaen.state.BasicState;
import org.cogaen.task.TaskService;
import org.cogaen.view.View;


import com.diamonddev.payup.game.entity.BuildingEntity;
import com.diamonddev.payup.game.entity.CarEntity;
import com.diamonddev.payup.game.entity.MapEntity;
import com.diamonddev.payup.game.entity.PlayerEntity;
import com.diamonddev.payup.game.entity.cars.RaceCarEntity;
import com.diamonddev.payup.game.event.EnterCarEvent;
import com.diamonddev.payup.game.event.ExitCarEvent;
import com.diamonddev.payup.game.event.InteractWithCarEvent;
import com.diamonddev.payup.game.hud.CompassHud;
import com.diamonddev.payup.game.hud.GameTimeHud;
import com.diamonddev.payup.game.hud.LevelHud;
import com.diamonddev.payup.game.hud.MapHud;
import com.diamonddev.payup.game.hud.MissionHud;
import com.diamonddev.payup.game.service.GameTimeService;
import com.diamonddev.payup.game.service.MapService;
import com.diamonddev.payup.game.tasks.NPCTask;
import com.diamonddev.payup.game.util.Log;


public class PlayState extends BasicState implements EventListener {

	public static final int DISPLAY_WIDTH = 50;
	public static final int WORLD_WIDTH = 500;
	public static final int WORLD_HEIGHT = 500;
	public static final CogaenId GAME_OVER = new CogaenId("GameOver");
	
	public static final CogaenId ID = new CogaenId("GameState");
	public static final CogaenId PLAYER_ENTITY_ID = new CogaenId("Player1");
	public static final CogaenId DEBTS_REPAYED = new CogaenId("DebtsRepayed");
	private View view;
	private PlayerEntity player;

	public PlayState(Core core) {
		super(core);
		view = new PlayView(core);
		ResourceService resSrv = ResourceService.getInstance(getCore());
		resSrv.createGroup(ID);
		this.view.registerResources(ID);
	}

	//***** EVENTHANDLING *****//
	@Override
	public void handleEvent(Event event) {
	}

	@Override
	public void onEnter() {
		super.onEnter();
		view.engage();
		PhysicsService.getInstance(getCore()).setGravity(0, 0);
		player = new PlayerEntity(getCore(), PLAYER_ENTITY_ID,200,200);
		EntityService.getInstance(getCore()).addEntity(player);
		
		MapService.getInstance(getCore()).initMap();
		CompassHud compassHud = new CompassHud(getCore());
		compassHud.registerResources(ID);
		compassHud.engage();
		
		LevelHud levelHud = new LevelHud(getCore());
		levelHud.registerResources(ID);
		levelHud.engage();
		
		GameTimeHud timeHud = new GameTimeHud(getCore());
		timeHud.registerResources(ID);
		timeHud.engage();
		
		MapHud mapView = new MapHud(getCore());
		mapView.registerResources(ID);
		mapView.engage();
		
		MissionHud missionHud = new MissionHud(getCore());
		missionHud.registerResources(ID);
		missionHud.engage();
		
		
		//Start GameTime
		GameTimeService.getInstance(getCore()).startClock();
		
		//Create new NPCTask
//		TaskService.getInstance(getCore()).attachTask(new NPCTask(getCore()));
		
		
	}

	@Override
	public void onExit() {
		view.disengage();
		super.onExit();
	}

}
