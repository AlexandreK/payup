package com.diamonddev.payup.game.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.cogaen.core.Core;
import org.cogaen.core.UpdateableService;
import org.cogaen.logging.LoggingService;
import org.cogaen.name.CogaenId;
import org.cogaen.util.Bag;

import com.diamonddev.payup.game.entity.Collidable;


public class CollisionService extends UpdateableService{
	
	private static final CogaenId ID = new CogaenId("CollisionService");
	private Bag<Collidable> passiveColliders;
	private Bag<Collidable> activeColliders;

	public CollisionService(){
		super();
		passiveColliders = new Bag<Collidable>();
		activeColliders = new Bag<Collidable>();
	}
	
	public static CollisionService getInstance(Core core) {
		return (CollisionService) core.getService(ID);
	}


	public void appendActiveCollidable(Collidable collider,CogaenId typeId){
		activeColliders.add(collider);
	}
	public void removeActiveCollidable(Collidable collider){
		activeColliders.remove(collider);
	}
	public void appendPassiveCollidable(Collidable collider,CogaenId typeId){
		passiveColliders.add(collider);
	}
	public void removePassiveCollidable(Collidable collider){
		passiveColliders.remove(collider);
	}
	public void stopColliding(Collidable collider, CogaenId typeId){
		
	}
	
	public void checkCollisions(){
		Collidable a;
		Collidable p;
		while(activeColliders.hasNext()){
			a = activeColliders.next();
			passiveColliders.reset();
			while(passiveColliders.hasNext()){
				p = passiveColliders.next();
				if(a!=null && p!=null&& a.getBoundingBox().collidesWith(p.getBoundingBox())){
					a.handleCollision(p);
					p.handleCollision(a);
				}
			}
		}
		activeColliders.reset();
	}

	@Override
	public void update() {
		checkCollisions();
	}

	@Override
	public CogaenId getId() {
		return ID;
	}

	@Override
	public String getName() {
		return "CollisionService";
	}

}
