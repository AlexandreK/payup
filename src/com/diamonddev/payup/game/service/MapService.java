package com.diamonddev.payup.game.service;

//import java.awt.Color;
//import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

//import org.cogaen.core.AbstractService;
import org.cogaen.box2d.Pose2D;
import org.cogaen.core.Core;
import org.cogaen.core.UpdateableService;
//import org.cogaen.entity.Entity;
import org.cogaen.entity.EntityService;
//import org.cogaen.event.EventService;
//import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.scene.Camera;
import org.cogaen.lwjgl.scene.SceneService;
import org.cogaen.math.Vector2;
import org.cogaen.name.CogaenId;
import org.cogaen.property.PropertyService;

import com.diamonddev.payup.game.PlayState;
import com.diamonddev.payup.game.ai.StarAlgorithm;
//import com.diamonddev.payup.game.entity.BuildingEntity;
import com.diamonddev.payup.game.entity.MapElement;
import com.diamonddev.payup.game.entity.textures.RoadEntity;
//import com.diamonddev.payup.game.event.PoseUpdateEvent;
//import com.diamonddev.payup.game.util.Log;
import com.diamonddev.payup.game.util.Log;
import com.diamonddev.payup.game.util.MapGenerator;
//import com.diamonddev.payup.game.util.WayPath;

public class MapService extends UpdateableService {

	public static final CogaenId ID = new CogaenId("MapService");
	private static final int MIN_GOAL_DISTANCE = 50;
	private static final int LEVEL_RANGE = 100;
	private MapElement[][] map = new MapElement[PlayState.WORLD_WIDTH][PlayState.WORLD_HEIGHT];
	private Camera camera;
	private List<CogaenId> visible = new ArrayList<CogaenId>();
	private StarAlgorithm pathService;

	public static MapService getInstance(Core core) {
		return (MapService) core.getService(ID);
	}

	
	public Vector2 getMissionGoal(Vector2 position, int level){
		boolean foundGoal = false;
		int x= 0,y = 0;
		int range = level * LEVEL_RANGE;
		Random r = new Random();
		while(!foundGoal){
			x = (int)position.x + (MIN_GOAL_DISTANCE+r.nextInt(range) - range/2);
			y = (int)position.y + (MIN_GOAL_DISTANCE+r.nextInt(range) - range/2);
			foundGoal = (x>=0&&y>=0&&x<PlayState.WORLD_WIDTH&&y<PlayState.WORLD_HEIGHT&&map[x][y].getType().equals(RoadEntity.TYPE_ID));
		}
		return new Vector2(x,y);
	}
	public List<Vector2> getPath(Vector2 start, Vector2 goal) {
		return pathService.getPath(start, goal);
	}

	@Override
	public CogaenId getId() {
		return ID;
	}

	@Override
	public String getName() {
		return "MapService";
	}

	public void initMap() {
		// createMap();

		loadMapData();
	}
 
	@Override
	public void update() {
		updateViewPort();

	}

	public void setActiveCamera(Camera camera) {
		this.camera = camera;

	}

	public Vector2 getSpawnPoint() {
		int minX, maxX, minY, maxY;
		int[] bounds = getBounds();
		Random r = new Random();
		minX = bounds[0] - 1;
		minY = bounds[1] - 1;
		maxX = bounds[2] + 1;
		maxY = bounds[3] + 1;
		Vector2 spawnPoint = null;
		while (spawnPoint == null) {
			for (int i = minX; i <= maxX; i++) {
				if (i > 0 && i < map.length && minY > 0 && r.nextInt(10) == 3
						&& map[i][minY].getType().equals(RoadEntity.TYPE_ID)) {
					spawnPoint = new Vector2(i, minY);
					break;
				}
				if (i > 0 && i < map.length && maxY < map[0].length
						&& r.nextInt(10) == 3
						&& map[i][maxY].getType().equals(RoadEntity.TYPE_ID)) {
					spawnPoint = new Vector2(i, maxY);
					break;
				}
			}
			for (int i = minY; i <= maxY; i++) {
				if (i > 0 && i < map[0].length && minX > 0
						&& r.nextInt(10) == 3
						&& map[minX][i].getType().equals(RoadEntity.TYPE_ID)) {
					spawnPoint = new Vector2(minX, i);
					break;
				}
				if (i > 0 && i < map[0].length && maxX < map.length
						&& r.nextInt(10) == 3
						&& map[maxX][i].getType().equals(RoadEntity.TYPE_ID)) {
					spawnPoint = new Vector2(maxX, i);
					break;
				}
			}
			return spawnPoint;
		}
		return null;
	}

	public int[] getBounds() {
		if (camera != null) {
			SceneService scn = SceneService.getInstance(getCore());
			int minX, maxX, minY, maxY;
			double cameraX = camera.getPosX();
			double cameraY = camera.getPosY();
			double zoom = camera.getZoom();

			double screenWidth = scn.getScreenWidth();
			double screenHeight = scn.getScreenHeight();

			double width = screenWidth / zoom;
			double height = screenHeight / zoom;

			minX = (int) Math.floor(cameraX - (width / 2)) - 1;
			maxX = (int) Math.floor(cameraX + (width / 2)) + 5;
			minY = (int) Math.floor(cameraY - (height / 2)) - 1;
			maxY = (int) Math.floor(cameraY + (height / 2)) + 5;

			if (minX < 0) {
				minX = 0;
			}
			if (minY < 0) {
				minY = 0;
			}
			if (maxX >= map.length) {
				maxX = map.length - 1;
			}
			if (maxY >= map[0].length) {
				maxY = map[0].length - 1;
			}
			int[] vals = new int[4];
			vals[0] = minX;
			vals[1] = minY;
			vals[2] = maxX;
			vals[3] = maxY;

			return vals;
		}
		return null;
	}

	public void updateViewPort() {
		EntityService e = EntityService.getInstance(getCore());
		int minX, maxX, minY, maxY;
		if (camera != null) {
			int[] bounds = getBounds();
			minX = bounds[0];
			minY = bounds[1];
			maxX = bounds[2];
			maxY = bounds[3];
			List<CogaenId> nowVisible = new ArrayList<CogaenId>();

			for (int i = minX; i < maxX; i++) {
				for (int j = minY; j < maxY; j++) {
					if (map[i][j] != null) {
						if (!map[i][j].isEngaged()) {
							e.addEntity(map[i][j]);
							Log.log(map[i][j].getX()+"/"+map[i][j].getY());
							// pose =
							// (Pose2D)map[i][j].getAttribute(Pose2D.ATTR_ID);
							// Log.log(pose.getPosX() +":"+ pose.getPosY());
							// EventService.getInstance(getCore()).dispatchEvent(new
							// PoseUpdateEvent(map[i][j].getId(),pose));
						}
						nowVisible.add(map[i][j].getId());
						visible.remove(map[i][j].getId());
					}
				}
			}

			if (visible.size() > 0) {
				for (CogaenId id : visible) {
//						Entity entityToRemove = e.getEntity(id);
					if (e.hasEntity(id)) {
//						e.removeEntity(id);
					}
				}
			}
			visible = nowVisible;

		}

	}

	public void loadMapData() {
		String path = PropertyService.getInstance(getCore()).getProperty(
				"mapPath",
				"res/map.gif");
		File file = new File(path);
		MapGenerator mg = new MapGenerator(getCore());
		try {
			BufferedImage img = ImageIO.read(file);
			for (int x = 0; x < img.getWidth(); x++) {
				for (int y = 0; y < img.getHeight(); y++) {
					if (map[x][y] == null) {
						int color = img.getRGB(x, y);
						mg.setColor(color);
						MapElement element = mg.getEnitity(x,y);
						addMapElement(element, x, y);
//						y += element.getHeight();
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		pathService = new StarAlgorithm(map, RoadEntity.TYPE_ID);
	}

	private void addMapElement(MapElement element, int x, int y) {
		for (int i = 0; i < element.getWidth(); i++) {
			for (int j = 0; j < element.getHeight(); j++) {
				if(x+i<500&y+j<500){
				if (map[x + i][y + j] == null) {
					map[x + i][y + j] = element;
				}
				}
			}
		}

//		Pose2D pose = (Pose2D) element.getAttribute(Pose2D.ATTR_ID);
//		double addX = ((double) element.getWidth()) / 2;//>1?((double) element.getWidth()) / 2:0;
//		double addY = ((double) element.getHeight()) / 2;//>1?((double) element.getHeight()) / 2:0;
//		double posX = ((double) x) + addX - 0.5;
//		double posY = ((double) y) + addY-0.5;
//		pose.setPosition(posX, posY);

	}
}
