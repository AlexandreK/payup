package com.diamonddev.payup.game.service;

import org.cogaen.core.Core;
import org.cogaen.core.UpdateableService;
import org.cogaen.event.Event;
import org.cogaen.event.EventListener;
import org.cogaen.event.EventService;
import org.cogaen.event.SimpleEvent;
import org.cogaen.name.CogaenId;
import org.cogaen.task.AbstractTask;
import org.cogaen.time.TimeService;
import org.cogaen.time.Timer;

import com.diamonddev.payup.game.PlayState;
import com.diamonddev.payup.game.event.GameOverEvent;
import com.diamonddev.payup.game.event.GameTimeEvent;
import com.diamonddev.payup.game.util.Log;

public class GameTimeService extends UpdateableService implements EventListener {
	
	private static final CogaenId ID = new CogaenId("GameTimeService");
	Timer gameClock;
	double endTime = 0.0;
	boolean timerStarted = false;
	private double timePerLevel = 50;
	private int timeleft;
	
	public GameTimeService(){
	}
	
	@Override
	public void handleEvent(Event event) {
		
	}

	public void startClock(){
		gameClock = TimeService.getInstance(getCore()).createTimer(new CogaenId("GameClock"));
		endTime = gameClock.getTime() + timePerLevel;
		timerStarted = true;
	}

	@Override
	public void update() {
		
		if(timerStarted ){
			int timeLeftTmp = (int) (endTime - gameClock.getTime());
			if(timeLeftTmp!=timeleft){
				timeleft = timeLeftTmp;
				EventService.getInstance(getCore()).dispatchEvent(new GameTimeEvent(timeleft));
			}
			if(endTime<gameClock.getTime()){
				//Zeit ausgelaufen
				EventService.getInstance(getCore()).dispatchEvent(new SimpleEvent(PlayState.GAME_OVER));
			}
		}
		
	}
	public void incrementTime(){
		this.endTime += timePerLevel ;
	}

	@Override
	public CogaenId getId() {
		return ID;
	}


	@Override
	public String getName() {
		return "GameTimeService";
	}
	
	public static GameTimeService getInstance(Core core) {
		return (GameTimeService) core.getService(ID);
	}

}
