package com.diamonddev.payup.game.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cogaen.core.Core;
import org.cogaen.core.UpdateableService;
import org.cogaen.event.EventService;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.event.LevelChangedEvent;
import com.diamonddev.payup.game.logic.Level;

public class LevelService extends UpdateableService{

	private static final CogaenId ID = new CogaenId("LevelService");
	
	private List<Level> levels;
	private Iterator<Level> i;
	
	public LevelService(){
		super();
		levels = new ArrayList<Level>();
		loadLevels();
		i = levels.iterator();
	}

	private void loadLevels() {
		
		//TODO: Read from File
		Level l1 = new Level();
		l1.setX(150);
		l1.setY(350);
		l1.setReward(10);
		l1.setText("Suche das Krankenhaus auf");
		l1.setTitle("Das Krankenhaus");
		
		Level l2 = new Level();
		l2.setX(255);
		l2.setY(255);
		l2.setReward(30);
		l2.setText("Fahre an den Strand");
		l2.setTitle("Der Strand");
		
		levels.add(l1);
		levels.add(l2);
		
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	public Level getLevel(){
		if(i.hasNext()){
			return i.next();
		}
		return null;
	}
	
	@Override
	public CogaenId getId() {
		return ID;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "LevelService";
	}
	
	public static LevelService getInstance(Core core) {
		return (LevelService) core.getService(ID);
	}

}
