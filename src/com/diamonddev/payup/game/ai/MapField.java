package com.diamonddev.payup.game.ai;

import org.cogaen.math.Vector2;


public class MapField {

	
	private Vector2 position;
	private int costs;
	private int expected;
	private int index;
	private MapField predecessor;

	
	public MapField() {
		this.position = new Vector2(0,0);
		this.costs = 0;
		this.expected = 0;
		this.index = 0;
	}
	
	
	public MapField(Vector2 p) {
		this.position = new Vector2(p);
		this.costs = 0;
		this.expected = 0;
		this.index = 0;
	}
	
	
	public void validateField(MapField current, MapField goal) {
		setExpected(goal);
		setCosts(current);
		index = costs + expected;
	}
	
	
	private void setExpected(MapField goal) {
		this.expected = getDiff(goal.position.x, position.x)*10 + getDiff(goal.position.y, position.y)*10;
	}
	
	
	private void setCosts(MapField current) {
		this.costs = current.costs + ((getDiff(current.position.x, position.x) > 0 && getDiff(current.position.y, position.y) > 0) ? 14 : 10);
	}
	
	
	public Boolean hasBetterCosts(MapField current) {
		return (costs < (current.costs + ((getDiff(current.position.x, position.x) > 0 && getDiff(current.position.y, position.y) > 0) ? 14 : 10)));
	}
	
	
	private int getDiff(double v1, double v2) {
		return (int) Math.abs(Math.abs(v1) - Math.abs(v2));
	}
	
	
	public MapField getPredecessor() {
		return this.predecessor;
	}
	
	
	public void setPredecessor(MapField predecessor) {
		this.predecessor = predecessor;
	}
	
	
	public double getX() {
		return position.x;
	}
	
	
	public double getY() {
		return position.y;
	}
	
	
	public Vector2 getPosition() {
		return position;
	}
	
	
	public int getCosts() {
		return costs;
	}

	
	public int getExpected() {
		return expected;
	}

	
	public int getIndex() {
		return index;
	}
}
