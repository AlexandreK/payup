package com.diamonddev.payup.game.ai;

import java.util.ArrayList;
import java.util.List;
import org.cogaen.math.Vector2;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.entity.BuildingEntity;
import com.diamonddev.payup.game.entity.MapElement;
import com.diamonddev.payup.game.entity.textures.RoadEntity;
import com.diamonddev.payup.game.util.Log;

public class StarAlgorithm {

	private MapElement[][] mapFields;
	private List<MapField> openFields;
	private List<MapField> pathFields;
	private List<MapField> toCheckFields;
	private Vector2[] sequence;
	private CogaenId walkableEntity;
	
	public StarAlgorithm(MapElement[][] map, CogaenId walkable){
		walkableEntity = walkable;
		mapFields = map;
	}
	
	public List<Vector2> getPath(Vector2 start, Vector2 goal) {
		
		openFields = new ArrayList<MapField>();
		pathFields = new ArrayList<MapField>();
		toCheckFields = new ArrayList<MapField>();
		sequence = new Vector2[8];
		sequence[0] = new Vector2(-1,1);
		sequence[1] = new Vector2(0,1);
		sequence[2] = new Vector2(1,1);
		sequence[3] = new Vector2(1,0);
		sequence[4] = new Vector2(1,-1);
		sequence[5] = new Vector2(0,-1);
		sequence[6] = new Vector2(-1,-1);
		sequence[7] = new Vector2(-1,0);
				
		MapField currentField = new MapField(start);
		MapField goalField = new MapField(goal);
		Vector2 currentPoint = new Vector2(0,0);
		MapField checkField = new MapField(currentPoint);
		
		
		openFields.add(currentField);
		
		do {
			
			toCheckFields.clear();
			
			for (int i=0; i < 8; ++i) {
				currentPoint.x = currentField.getX() + sequence[i].x;
				currentPoint.y = currentField.getY() + sequence[i].y;
				
				if (isPossibleOnMapField(currentPoint, sequence[i]) && !isAlreadyAPathField(currentPoint)) {
					if (isInOpenFieldList(currentPoint)) {
						checkField = getOpenField(currentPoint);
						
						if (checkField.hasBetterCosts(currentField)) {
							checkField.setPredecessor(currentField);
							checkField.validateField(currentField, goalField);
						}
					} else {
						checkField = new MapField(currentPoint);
						checkField.setPredecessor(currentField);
						checkField.validateField(currentField, goalField);
						openFields.add(checkField);
					}
					
					toCheckFields.add(checkField);
				}
			}
			
			MapField nearestField = new MapField();
			for (MapField field : toCheckFields) {
				if (field.getIndex() < nearestField.getIndex() || nearestField.getIndex() == 0) {
					nearestField = field;
				}
			}
			
			openFields.remove(currentField);
			pathFields.add(currentField);
			currentField = nearestField;
			
		} while (!isAlreadyAPathField(goalField.getPosition()) && !toCheckFields.isEmpty());
		
		
		
		List<Vector2> pathVector2s = new ArrayList<Vector2>();
		
		if (toCheckFields.isEmpty()) {
//			System.out.println("THERE IS NO WAY TO GET TO THE GOAL!");
		} else {
//			System.out.println("I FOUND A WAY TO THE GOAL!");
			for (MapField field : pathFields) pathVector2s.add(field.getPosition());
		}
		
	
		return pathVector2s;
	}
	
	
	private boolean isPossibleOnMapField(Vector2 currentPoint, Vector2 sequenceVector2) {
		boolean isPossible = false;
		if (mapContains(currentPoint) && !isAnObstacle(currentPoint)) {
			if (sequenceVector2.x != 0 && sequenceVector2.y != 0) {
				Vector2 testPoint1 = (sequenceVector2.x == -1) ? new Vector2(currentPoint.x+1, currentPoint.y) : new Vector2(currentPoint.x-1, currentPoint.y) ;
				Vector2 testPoint2 = (sequenceVector2.y == -1) ? new Vector2(currentPoint.x, currentPoint.y+1) : new Vector2(currentPoint.x, currentPoint.y-1) ;

				if (mapContains(testPoint1) && mapContains(testPoint2)) {
					isPossible = (isAnObstacle(testPoint1) || isAnObstacle(testPoint2)) ? false : true;
				}
			} else {
				isPossible = true;
			}
		}
		
		return isPossible;
	}
	
	
	private boolean mapContains(Vector2 currentPoint) {
		return (currentPoint.x >=0 &&currentPoint.y >=0 && currentPoint.x < mapFields.length && currentPoint.y < mapFields[0].length);
	}
	private boolean isAnObstacle(Vector2 testPoint) {
		boolean result = !((mapFields[(int) testPoint.x][(int) testPoint.y].getType().equals(walkableEntity)));
		if(result){
//			Log.log("Obstacle");
		}
		return result;
	}
	
	
	private boolean isAlreadyAPathField(Vector2 currentPoint) {
		boolean isPathField = false;
		for (MapField field : pathFields) {
			if (field.getX() == currentPoint.x && field.getY() == currentPoint.y) {
				isPathField = true;
				break;
			}
		}
		
		return isPathField;
	}
	
	
	private boolean isInOpenFieldList(Vector2 currentPoint) {
		boolean isInList = false;
		for (MapField field : openFields) {
			if (field.getX() == currentPoint.x && field.getY() == currentPoint.y) {
				isInList = true;
				break;
			}
		}
		
		return isInList;
	}
	
	
	private MapField getOpenField(Vector2 point) {
		MapField openField = new MapField();
		for (MapField field : openFields) {
			if (field.getX() == point.x && field.getY() == point.y) {
				openField = field;
				break;
			}
		}
		return openField;
	}

}
