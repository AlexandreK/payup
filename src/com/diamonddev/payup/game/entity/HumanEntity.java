package com.diamonddev.payup.game.entity;

import org.cogaen.box2d.BodyComponent;
import org.cogaen.box2d.PolygonShapeComponent;
import org.cogaen.box2d.Pose2D;
import org.cogaen.core.Core;
import org.cogaen.entity.ComponentEntity;
import org.cogaen.event.EventService;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.input.ControllerComponent;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.component.CollisionComponent;
import com.diamonddev.payup.game.component.LifecycleComponent;
import com.diamonddev.payup.game.component.StaticPoseComponent;
import com.diamonddev.payup.game.event.CollisionListener;
import com.diamonddev.payup.game.util.BoundingBox;
import com.diamonddev.payup.game.util.Collision;

public class HumanEntity extends ComponentEntity {

	public static final double BODY_WIDTH = 1.0;
	public static final double BODY_THICKNESS = 0.8;
	public static final double HEAD_RADIUS = 0.6;
	public static final Object TYPE_ID = new CogaenId("HumanEntity");
	public static final float BODY_MASS = 75.0f;
	
	public HumanEntity(Core core, CogaenId id, CogaenId typeId,double x, double y) {
		super(core, id, typeId);
		
		
		addComponent(new LifecycleComponent());
	}

	public static double getSpeed(boolean running){
		if(running){
		return 15.0;
		} else {
			return 5.0;
		}
	}
}
