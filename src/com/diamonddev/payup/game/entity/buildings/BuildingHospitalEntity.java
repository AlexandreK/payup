package com.diamonddev.payup.game.entity.buildings;

import org.cogaen.core.Core;

import com.diamonddev.payup.game.entity.BuildingEntity;

public class BuildingHospitalEntity extends BuildingEntity {

	@Override
	public String getTexture() {
		// TODO Auto-generated method stub
		return "hospital";
	}

	public BuildingHospitalEntity(Core core, double x, double y) {
		super(core,x,y);
	}

}
