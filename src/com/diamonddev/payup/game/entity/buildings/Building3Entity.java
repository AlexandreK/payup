package com.diamonddev.payup.game.entity.buildings;

import org.cogaen.core.Core;

import com.diamonddev.payup.game.entity.BuildingEntity;

public class Building3Entity extends BuildingEntity {

	@Override
	public String getTexture() {
		// TODO Auto-generated method stub
		return "building_3";
	}

	public Building3Entity(Core core, double x, double y) {
		super(core,x,y);
	}

}
