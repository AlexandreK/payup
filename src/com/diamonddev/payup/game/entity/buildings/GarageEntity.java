package com.diamonddev.payup.game.entity.buildings;

import org.cogaen.core.Core;

import com.diamonddev.payup.game.entity.BuildingEntity;

public class GarageEntity extends BuildingEntity {

	public GarageEntity(Core core, double x, double y) {
		super(core,x,y);
	}

}
