package com.diamonddev.payup.game.entity.humans;

import org.cogaen.core.Core;
import org.cogaen.event.EventService;
import org.cogaen.name.CogaenId;
import org.cogaen.name.IdService;

import com.diamonddev.payup.game.component.KIComponent;
import com.diamonddev.payup.game.component.StaticPoseComponent;
import com.diamonddev.payup.game.component.TimeToLiveComponent;
import com.diamonddev.payup.game.entity.Collidable;
import com.diamonddev.payup.game.entity.HumanEntity;
import com.diamonddev.payup.game.event.CollisionEvent;
import com.diamonddev.payup.game.event.CollisionListener;
import com.diamonddev.payup.game.util.BoundingBox;
import com.diamonddev.payup.game.util.Collision;

public class PedestrianEntity extends HumanEntity{

	public static final CogaenId TYPE_ID = new CogaenId("PedestrianEntity");
	private static final double TIME_TO_LIVE = 50.0;

	public PedestrianEntity(Core core, double x, double y) {
		super(core, IdService.getInstance(core).generateId(), TYPE_ID,x,y);
		addComponent(new TimeToLiveComponent(TIME_TO_LIVE));
		addComponent(new StaticPoseComponent());
		addComponent(new KIComponent());
		
	}

}
