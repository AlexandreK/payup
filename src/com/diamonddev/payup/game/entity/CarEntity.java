package com.diamonddev.payup.game.entity;

import org.cogaen.core.Core;
import org.cogaen.entity.ComponentEntity;
import org.cogaen.lwjgl.input.ControllerComponent;
import org.cogaen.name.CogaenId;
import org.cogaen.name.IdService;

import com.diamonddev.payup.game.component.LifecycleComponent;
import com.diamonddev.payup.game.component.StaticPoseComponent;

public abstract class CarEntity extends ComponentEntity {
	
	public static final CogaenId TYPE_ID = new CogaenId("CarEntity");
	@Override
	public CogaenId getType() {
		return TYPE_ID;
	}

	public static final double WIDTH = 1.0;
	public static final double HEIGHT = 2.0;

	public abstract double getSpeed();
	
	public CarEntity(Core core) {
		super(core, IdService.getInstance(core).generateId(), TYPE_ID);
		addComponent(new LifecycleComponent());
		addComponent(new StaticPoseComponent());
		addComponent(new ControllerComponent(1));
	}

}
