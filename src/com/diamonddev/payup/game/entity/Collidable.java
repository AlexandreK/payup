package com.diamonddev.payup.game.entity;

import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.util.BoundingBox;


public interface Collidable {

	public BoundingBox getBoundingBox();
	public void handleCollision(Collidable c);
	
}
