package com.diamonddev.payup.game.entity;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

import org.cogaen.box2d.BodyComponent;
import org.cogaen.box2d.PolygonShapeComponent;
import org.cogaen.core.Core;
import org.cogaen.entity.ComponentEntity;
import org.cogaen.entity.EntityService;
import org.cogaen.logging.LoggingService;
import org.cogaen.name.CogaenId;
import org.cogaen.property.PropertyService;

import com.diamonddev.payup.game.PlayState;
import com.diamonddev.payup.game.PlayView;
import com.diamonddev.payup.game.component.LifecycleComponent;
import com.diamonddev.payup.game.component.StaticPoseComponent;

public class MapEntity extends ComponentEntity {
	

	private MapElement[][] map = new MapElement[PlayState.WORLD_WIDTH][PlayState.WORLD_HEIGHT];

	public static final CogaenId TYPE_ID = new CogaenId("MapEntity");
	public MapEntity(Core core, CogaenId id,double x, double y) {
		super(core, id, TYPE_ID);
		addComponent(new LifecycleComponent());
//		addComponent(new StaticPoseComponent());
	}
	@Override
	public void engage() {
		super.engage();
		double width =PlayState.WORLD_WIDTH;
//		createBuildings((int)width);
	}
//	private void createBuildings(int width) {
//		//TODO:Create Map Building Algorithm;
//		EntityService e = EntityService.getInstance(getCore());
//		LoggingService l = LoggingService.getInstance(getCore());
//		Random r = new Random();
//		Pose2D pose;
//		int build = 0;
//			for(int x=0; x<map.length;x+=5){
//				for(int y=0;y<map[x].length;y+=5){
//					build = r.nextInt(6);
//					if(build == 3){
//						map[x][y] = new BuildingEntity(getCore());
//						for(int i=x;i<x+BuildingEntity.WIDTH;i++){
//							for(int j=x;j<y+BuildingEntity.WIDTH;j++){
//								map[i][j] = map[x][y];
//							}
//						}
//						pose = (Pose2D)map[x][y].getAttribute(Pose2D.ATTR_ID);
//						pose.setPosition(x-PlayState.WORLD_WIDTH/2, y-PlayState.WORLD_HEIGHT/2);
//						e.addEntity(map[x][y]);
//					}
//				}
//			}
//	}
	@Override
	public void disengage() {
		super.disengage();
	}

}
