package com.diamonddev.payup.game.entity;

import org.cogaen.box2d.BodyComponent;
import org.cogaen.box2d.PolygonShapeComponent;
import org.cogaen.core.Core;
import org.cogaen.entity.ComponentEntity;
import org.cogaen.lwjgl.input.ControllerComponent;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.component.LifecycleComponent;
import com.diamonddev.payup.game.component.MissionComponent;
import com.diamonddev.payup.game.component.SteeringComponent;
import com.diamonddev.payup.game.component.WheelComponent;


public class PlayerEntity extends ComponentEntity {

	public static final CogaenId TYPE_ID = new CogaenId("PlayerEntity");
	
	public static final CogaenId LEFT_FRONT_WHEEL_ID  = new CogaenId("LeftFrontWheel");
	public static final CogaenId RIGHT_FRONT_WHEEL_ID = new CogaenId("RightFrontWheel");
	public static final CogaenId LEFT_REAR_WHEEL_ID   = new CogaenId("LeftReatWheel");
	public static final CogaenId RIGHT_REAR_WHEEL_ID  = new CogaenId("RightReadWheel");
	
	public static final double WIDTH 				  = 2.0;
	public static final double HEIGHT                 = 4.0;
	
	
	public PlayerEntity(Core core, CogaenId id, double x, double y) {
		super(core, id, TYPE_ID);
		
		BodyComponent bodyCmp = new BodyComponent(x, y, 0, BodyComponent.Type.DYNAMIC);
		bodyCmp.setAngularDamping(0.1);
		bodyCmp.setLinearDamping(0.7);
		addComponent(bodyCmp);
		
		PolygonShapeComponent polyShape = new PolygonShapeComponent();
		polyShape.addVertex(-WIDTH/2, HEIGHT/2);
		polyShape.addVertex(-WIDTH/2, -HEIGHT/2);
		polyShape.addVertex(WIDTH/2, -HEIGHT/2);
		polyShape.addVertex(WIDTH/2, HEIGHT/2);
		addComponent(polyShape);
		
		addComponent(new WheelComponent(true,-WIDTH/2, HEIGHT/2));
		addComponent(new WheelComponent(true,WIDTH/2, HEIGHT/2));
		addComponent(new WheelComponent(false,-WIDTH/2, -HEIGHT/2));
		addComponent(new WheelComponent(false,WIDTH/2, -HEIGHT/2));
		
		
		addComponent(new ControllerComponent(1));
		addComponent(new LifecycleComponent());
		addComponent(new SteeringComponent());
		addComponent(new MissionComponent());
		
	}
	

}
