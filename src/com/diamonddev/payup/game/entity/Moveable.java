package com.diamonddev.payup.game.entity;

import org.cogaen.name.CogaenId;

public interface Moveable {

	public static final CogaenId ATTR_ID = new CogaenId("Moveable");
	
	public void setAngle(double angle);
	public void setSpeed(double speed);
}
