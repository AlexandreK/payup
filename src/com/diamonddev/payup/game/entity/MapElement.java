package com.diamonddev.payup.game.entity;

import org.cogaen.core.Core;
import org.cogaen.entity.ComponentEntity;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.component.LifecycleComponent;
import com.diamonddev.payup.game.component.StaticPoseComponent;

public abstract class MapElement  extends ComponentEntity {

	private double x, y;
	public MapElement(Core core, CogaenId id, CogaenId typeId,double x, double y) {
		super(core, id, typeId);
	}
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public int getWidth(){
		return 1;
	}
	public int getHeight(){
		return 1;
	}
		

}
