package com.diamonddev.payup.game.entity.textures;

import org.cogaen.box2d.Pose2D;
import org.cogaen.core.Core;
import org.cogaen.name.CogaenId;
import org.cogaen.name.IdService;

import com.diamonddev.payup.game.component.LifecycleComponent;
import com.diamonddev.payup.game.component.StaticPoseComponent;
import com.diamonddev.payup.game.entity.MapElement;

public class WaterEntity extends TextureEntity {

	public static final CogaenId TYPE_ID = new CogaenId("WaterEntity");

	public WaterEntity(Core core, double x, double y) {
		super(core, TYPE_ID,x,y);
	}
	
	public String getTexture(){
		return "water";
	}

}
