package com.diamonddev.payup.game.entity.textures;

import org.cogaen.box2d.BodyComponent;
import org.cogaen.box2d.PolygonShapeComponent;
import org.cogaen.box2d.Pose2D;
import org.cogaen.core.Core;
import org.cogaen.name.CogaenId;
import org.cogaen.name.IdService;

import com.diamonddev.payup.game.component.LifecycleComponent;
import com.diamonddev.payup.game.component.StaticPoseComponent;
import com.diamonddev.payup.game.entity.MapElement;

public class RoadEntity extends MapElement {

	public static final CogaenId TYPE_ID = new CogaenId("RoadEntity");
	
	public static final double SIZE = 1.0;

	public RoadEntity(Core core, double x, double y) {
		super(core, IdService.getInstance(core).generateId(),TYPE_ID,x,y);
		addComponent(new LifecycleComponent());
		addComponent(new StaticPoseComponent());
		Pose2D pose = (Pose2D) getAttribute(Pose2D.ATTR_ID);
		pose.setPosition(x-2, y-2);
	}
	@Override
	public CogaenId getType() {
		return TYPE_ID;
	}
	public int getWidth(){
		return 1;
	}
	public int getHeight(){
		return 1;
	}
}
