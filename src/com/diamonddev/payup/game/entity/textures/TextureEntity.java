package com.diamonddev.payup.game.entity.textures;

import org.cogaen.box2d.Pose2D;
import org.cogaen.core.Core;
import org.cogaen.name.CogaenId;
import org.cogaen.name.IdService;

import com.diamonddev.payup.game.component.LifecycleComponent;
import com.diamonddev.payup.game.component.StaticPoseComponent;
import com.diamonddev.payup.game.entity.MapElement;

public class TextureEntity extends MapElement {

	public static final CogaenId TYPE_ID = new CogaenId("TextureEntity");

	public TextureEntity(Core core, CogaenId typeId, double x, double y) {
		super(core, IdService.getInstance(core).generateId(), typeId, x, y);
		addComponent(new LifecycleComponent());
		addComponent(new StaticPoseComponent());
		Pose2D pose = (Pose2D) getAttribute(Pose2D.ATTR_ID);
		pose.setPosition(x - 2, y - 2);
	}
	
	public int getWidth(){
		return 1;
	}
	public int getHeight(){
		return 1;
	}
	
	public String getTexture(){
		return "sand";
	}

}
