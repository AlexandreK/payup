package com.diamonddev.payup.game.entity.textures;

import org.cogaen.box2d.BodyComponent;
import org.cogaen.box2d.PolygonShapeComponent;
import org.cogaen.box2d.Pose2D;
import org.cogaen.core.Core;
import org.cogaen.name.CogaenId;
import org.cogaen.name.IdService;

import com.diamonddev.payup.game.component.LifecycleComponent;
import com.diamonddev.payup.game.component.StaticPoseComponent;
import com.diamonddev.payup.game.entity.MapElement;

public class QuayEntity extends MapElement {

	public static final CogaenId TYPE_ID = new CogaenId("QuayEntity");

	public QuayEntity(Core core, double x, double y) {
		super(core, IdService.getInstance(core).generateId(), TYPE_ID,x,y);
		BodyComponent bodyCmp = new BodyComponent(x-2, y-2, 0, BodyComponent.Type.STATIC);
		addComponent(bodyCmp);
		
		PolygonShapeComponent polyShape = new PolygonShapeComponent();
		polyShape.addVertex(-0.5, 0.5);
		polyShape.addVertex(-0.5, -0.5);
		polyShape.addVertex(0.5, -0.5);
		polyShape.addVertex(0.5, 0.5);
		addComponent(polyShape);
		addComponent(new LifecycleComponent());
	}
	
	public int getWidth(){
		return 1;
	}
	public int getHeight(){
		return 1;
	}

}
