package com.diamonddev.payup.game.entity.cars;

import org.cogaen.core.Core;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.entity.CarEntity;

public class RaceCarEntity extends CarEntity {

	public RaceCarEntity(Core core) {
		super(core);
	}

	@Override
	public double getSpeed() {
		return 20.0;
	}
}
