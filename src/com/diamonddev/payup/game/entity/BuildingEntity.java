package com.diamonddev.payup.game.entity;

import org.cogaen.box2d.BodyComponent;
import org.cogaen.box2d.PolygonShapeComponent;
import org.cogaen.core.Core;
import org.cogaen.entity.ComponentEntity;
import org.cogaen.lwjgl.input.ControllerComponent;
import org.cogaen.name.CogaenId;
import org.cogaen.name.IdService;

import com.diamonddev.payup.game.component.CollisionComponent;
import com.diamonddev.payup.game.component.LifecycleComponent;
import com.diamonddev.payup.game.component.StaticPoseComponent;
import com.diamonddev.payup.game.event.CollisionListener;
import com.diamonddev.payup.game.util.BoundingBox;

public class BuildingEntity extends MapElement{

	public static final double WIDTH = 5.0;
	public static final double HEIGHT = 5.0;
	public static final CogaenId TYPE_ID = new CogaenId("BuildingEntity");

	public BuildingEntity(Core core,double x, double y) {
		super(core, IdService.getInstance(core).generateId(), TYPE_ID,x,y);
		BodyComponent bodyCmp = new BodyComponent(x, y, 0, BodyComponent.Type.STATIC);
		addComponent(bodyCmp);
		
		PolygonShapeComponent polyShape = new PolygonShapeComponent();
		polyShape.addVertex(-2.5, 2.5);
		polyShape.addVertex(-2.5, -2.5);
		polyShape.addVertex(2.5, -2.5);
		polyShape.addVertex(2.5, 2.5);
		addComponent(polyShape);
		
		addComponent(new LifecycleComponent());
	}


	public int getWidth(){
		return 5;
	}
	public int getHeight(){
		return 5;
	}
	public String getTexture(){
		return "building_1";
	}
}
