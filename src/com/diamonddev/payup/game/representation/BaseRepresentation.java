package com.diamonddev.payup.game.representation;


import org.cogaen.core.Core;
import org.cogaen.lwjgl.scene.SceneNode;
import org.cogaen.lwjgl.scene.SceneService;
import org.cogaen.name.CogaenId;
import org.cogaen.view.EntityRepresentation;

public class BaseRepresentation extends EntityRepresentation {

	private SceneNode node;
	
	public BaseRepresentation(Core core, CogaenId entityId) {
		super(core, entityId);
	}
	

	@Override
	public void engage() {
		super.engage();
		SceneService scnSrv = SceneService.getInstance(getCore());
		this.node = scnSrv.createNode();
		
		scnSrv.getLayer(2).addNode(this.node);
	}

	@Override
	public void disengage() {
		SceneService.getInstance(getCore()).destroyNode(this.node);
		super.disengage();
	}

	public void setPose(double x, double y, double angle) {
		this.node.setPose(x, y, angle);
	}
	
	public SceneNode getNode() {
		return this.node;
	}


	public void engageOnLayer(int i) {
		super.engage();
		SceneService scnSrv = SceneService.getInstance(getCore());
		this.node = scnSrv.createNode();
		
		scnSrv.getLayer(i).addNode(this.node);
		
	}

}
