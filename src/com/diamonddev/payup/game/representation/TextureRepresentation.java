package com.diamonddev.payup.game.representation;

import org.cogaen.core.Core;
import org.cogaen.entity.EntityService;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.scene.Color;
import org.cogaen.lwjgl.scene.ReadableColor;
import org.cogaen.lwjgl.scene.RectangleVisual;
import org.cogaen.lwjgl.scene.SpriteVisual;
import org.cogaen.lwjgl.scene.Visual;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.entity.textures.TextureEntity;
import com.diamonddev.payup.game.entity.PlayerEntity;
import com.diamonddev.payup.game.util.Log;

public class TextureRepresentation extends BaseRepresentation{

	public TextureRepresentation(Core core, CogaenId entityId) {
		super(core, entityId);
	}
	
	public void engage(){
		super.engageOnLayer(2);
		String texture = ((TextureEntity)EntityService.getInstance(getCore()).getEntity(getEntityId())).getTexture();
		SpriteVisual pavement = new SpriteVisual(getCore(),texture,1,1);
		getNode().addVisual(pavement);
	}

}
