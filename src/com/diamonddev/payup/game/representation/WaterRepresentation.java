package com.diamonddev.payup.game.representation;

import org.cogaen.core.Core;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.scene.Color;
import org.cogaen.lwjgl.scene.ReadableColor;
import org.cogaen.lwjgl.scene.RectangleVisual;
import org.cogaen.lwjgl.scene.SpriteVisual;
import org.cogaen.lwjgl.scene.Visual;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.entity.BuildingEntity;
import com.diamonddev.payup.game.entity.PlayerEntity;
import com.diamonddev.payup.game.util.Log;

public class WaterRepresentation extends BaseRepresentation{

	public WaterRepresentation(Core core, CogaenId entityId) {
		super(core, entityId);
	}
	
	public void engage(){
		super.engageOnLayer(2);
		SpriteVisual pavement = new SpriteVisual(getCore(),"water",1,1);
//		pavement.setColor(Color.RED);
		getNode().addVisual(pavement);
	}

}
