package com.diamonddev.payup.game.representation;

import org.cogaen.core.Core;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.scene.CircleVisual;
import org.cogaen.lwjgl.scene.Color;
import org.cogaen.lwjgl.scene.RectangleVisual;
import org.cogaen.lwjgl.scene.SpriteVisual;
import org.cogaen.lwjgl.scene.Visual;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.entity.CarEntity;
import com.diamonddev.payup.game.entity.PlayerEntity;
import com.diamonddev.payup.game.entity.humans.PedestrianEntity;


public class HumanRepresentation extends BaseRepresentation {

	public HumanRepresentation(Core core, CogaenId entityId) {
		super(core, entityId);
	}

	public void engage() {
		super.engage();
//		Visual body = new RectangleVisual(PedestrianEntity.BODY_WIDTH,PedestrianEntity.BODY_THICKNESS);
//		body.setColor(Color.WHITE);
//		getNode().addVisual(body);
//		Visual head = new RectangleVisual(PedestrianEntity.HEAD_RADIUS,PedestrianEntity.HEAD_RADIUS);
//		head.setColor(Color.RED);
//		getNode().addVisual(head);
		Visual car = new SpriteVisual(getCore(),"car",CarEntity.WIDTH,CarEntity.HEIGHT);
		getNode().addVisual(car);
		
	}

}
