package com.diamonddev.payup.game.representation;

import org.cogaen.core.Core;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.scene.Color;
import org.cogaen.lwjgl.scene.RectangleVisual;
import org.cogaen.lwjgl.scene.SpriteVisual;
import org.cogaen.lwjgl.scene.Visual;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.entity.CarEntity;
import com.diamonddev.payup.game.entity.PlayerEntity;

public class PlayerRepresentation extends BaseRepresentation {

	public PlayerRepresentation(Core core, CogaenId entityId) {
		super(core, entityId);
	}
	
	@Override
	public void engage() {
		super.engage();
//		Visual car = new RectangleVisual(PlayerEntity.WIDTH,PlayerEntity.HEIGHT);
//		car.setColor(Color.WHITE);
		Visual car = new SpriteVisual(getCore(),"car",CarEntity.WIDTH*2+0.1,CarEntity.HEIGHT*2+0.1);
		getNode().addVisual(car);
//		getNode().addVisual(car);
	}

	@Override
	public void disengage() {
		super.disengage();
	}

}
