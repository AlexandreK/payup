package com.diamonddev.payup.game.representation;

import java.util.Random;

import org.cogaen.core.Core;
import org.cogaen.entity.EntityService;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.scene.Color;
import org.cogaen.lwjgl.scene.ReadableColor;
import org.cogaen.lwjgl.scene.RectangleVisual;
import org.cogaen.lwjgl.scene.SpriteVisual;
import org.cogaen.lwjgl.scene.TextVisual;
import org.cogaen.lwjgl.scene.Visual;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.entity.BuildingEntity;
import com.diamonddev.payup.game.entity.PlayerEntity;

public class BuildingRepresentation extends BaseRepresentation {

	private TextVisual textVisual;

	public BuildingRepresentation(Core core, CogaenId entityId) {
		super(core, entityId);
	}

	public void engage() {
		super.engage();
		Random rdm = new Random();
		String texture = ((BuildingEntity)EntityService.getInstance(getCore()).getEntity(getEntityId())).getTexture();
		SpriteVisual house = new SpriteVisual(getCore(),texture ,5,5);
//		RectangleVisual house = new RectangleVisual(BuildingEntity.WIDTH,
//				BuildingEntity.HEIGHT);
//		ReadableColor c = randomHouse();
//		house.setColor(c);
////		house.setFilled(false);
		getNode().addVisual(house);

	}

	private ReadableColor randomHouse() {
		ReadableColor[] colors = new Color[5];
		colors[0] = new Color(0.5, 0.7, 0.00);
		colors[1] = new Color(0.5, 0.3, 0.10);
		colors[2] = new Color(0.2, 0.0, 0.60);
		colors[3] = new Color(0.8, 0.2, 0.60);
		colors[4] = new Color(0.8, 0.7, 0.60);
		int rand = (int) (Math.random() * 5);
		return colors[rand];
	}

}
