package com.diamonddev.payup.game.representation;

import org.cogaen.core.Core;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.scene.Color;
import org.cogaen.lwjgl.scene.ReadableColor;
import org.cogaen.lwjgl.scene.RectangleVisual;
import org.cogaen.lwjgl.scene.SpriteVisual;
import org.cogaen.lwjgl.scene.Visual;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.entity.BuildingEntity;
import com.diamonddev.payup.game.entity.CarEntity;

public class CarRepresentation extends BaseRepresentation {

	public CarRepresentation(Core core, CogaenId entityId) {
		super(core, entityId);
	}

	@Override
	public void engage() {
		super.engage();
		Visual car = new SpriteVisual(getCore(),"car",CarEntity.WIDTH,CarEntity.HEIGHT);
		getNode().addVisual(car);
	}

	@Override
	public void disengage() {
		super.disengage();
	}

}
