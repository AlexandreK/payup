package com.diamonddev.payup.game.representation;

import org.cogaen.core.Core;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.scene.Color;
import org.cogaen.lwjgl.scene.ReadableColor;
import org.cogaen.lwjgl.scene.RectangleVisual;
import org.cogaen.lwjgl.scene.SpriteVisual;
import org.cogaen.lwjgl.scene.Visual;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.entity.BuildingEntity;
import com.diamonddev.payup.game.entity.PlayerEntity;
import com.diamonddev.payup.game.util.Log;

public class SidewalkRepresentation extends BaseRepresentation{

	public SidewalkRepresentation(Core core, CogaenId entityId) {
		super(core, entityId);
	}
	
	public void engage(){
		super.engageOnLayer(3);
		SpriteVisual pavement = new SpriteVisual(getCore(),"pavement",1,1);
//		pavement.setColor(Color.RED);
		getNode().addVisual(pavement);
	}

}
