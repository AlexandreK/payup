package com.diamonddev.payup.game.representation;

import org.cogaen.core.Core;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.scene.Color;
import org.cogaen.lwjgl.scene.ReadableColor;
import org.cogaen.lwjgl.scene.RectangleVisual;
import org.cogaen.lwjgl.scene.TextVisual;
import org.cogaen.lwjgl.scene.Visual;
import org.cogaen.name.CogaenId;
import org.cogaen.resource.ResourceService;

import com.diamonddev.payup.game.entity.BuildingEntity;
import com.diamonddev.payup.game.entity.PlayerEntity;
import com.diamonddev.payup.game.entity.textures.RoadEntity;

public class RoadRepresentation extends BaseRepresentation{
	public RoadRepresentation(Core core, CogaenId entityId) {
		super(core, entityId);
//		super.setLayer(2);
	}
	
	public void engage(){
		super.engage();
//		super.engageOnLayer(3);
//		RectangleVisual house = new RectangleVisual(RoadEntity.SIZE,RoadEntity.SIZE);
//		house.setColor(new Color(0.2,0.2,0.2));
//		house.setFilled(false);
//		getNode().addVisual(house);
	}

}
