package com.diamonddev.payup.game.hud;

import org.cogaen.box2d.PoseUpdateEvent;
import org.cogaen.core.Core;
import org.cogaen.event.Event;
import org.cogaen.event.EventListener;
import org.cogaen.event.EventService;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.scene.Camera;
import org.cogaen.lwjgl.scene.CircleVisual;
import org.cogaen.lwjgl.scene.Color;
import org.cogaen.lwjgl.scene.RectangleVisual;
import org.cogaen.lwjgl.scene.SceneNode;
import org.cogaen.lwjgl.scene.SceneService;
import org.cogaen.lwjgl.scene.TextVisual;
import org.cogaen.lwjgl.scene.Visual;
import org.cogaen.name.CogaenId;
import org.cogaen.resource.ResourceService;
import org.cogaen.resource.TextHandle;
import org.cogaen.time.TimeService;
import org.cogaen.time.Timer;
import org.cogaen.view.AbstractHud;

import com.diamonddev.payup.game.event.CompassChangedEvent;
import com.diamonddev.payup.game.event.LevelEvent;
import com.diamonddev.payup.game.event.MapBoundsEvent;

public class MissionHud extends AbstractHud implements EventListener {

	private CogaenId resourceGrp;
	
	SceneNode node;
	SceneService scene;
	TextVisual mission;
	Timer t;
	

	public MissionHud(Core core) {
		super(core);
		EventService.getInstance(getCore()).addListener(this, LevelEvent.TYPE_ID);
		EventService.getInstance(getCore()).addListener(this, PoseUpdateEvent.TYPE_ID);
		t = TimeService.getInstance(core).createTimer(new CogaenId("MissionHudTimer"));
	}

	@Override
	public void registerResources(CogaenId groupId) {
		if(!ResourceService.getInstance(getCore()).isDeclared("scoreTxt")){
		this.resourceGrp = groupId;
		ResourceService serSrv =ResourceService.getInstance(getCore());
		serSrv.declareResource("scoreTxt", groupId, new TextHandle("scoreTxt"));
		}
	}

	@Override
	public void engage() {
		super.engage();
		scene = SceneService.getInstance(getCore());
		
		node = scene.createNode();
		
		mission = new TextVisual(getCore(),"HudFnt","");
		mission.setColor(Color.WHITE);
		mission.setScale(0.03);
		Camera camera = scene.getCamera(0);
		node.addVisual(mission);
		node.setPose(camera.getPosX()-10, camera.getPosY()-10, 0);
		
		
		scene.getLayer(0).addNode(node);
		
	}


	@Override
	public void disengage() {
		super.disengage();
	}

	@Override
	public void handleEvent(Event event) {
		if(event.getTypeId().equals(PoseUpdateEvent.TYPE_ID)){
			Camera camera = scene.getCamera(0);
			node.setPose(camera.getPosX()-25, camera.getPosY()-18,0);
		} else if(event.getTypeId().equals(LevelEvent.TYPE_ID)){
			LevelEvent evt = (LevelEvent) event;
			mission.setText(evt.getLevel().getText());
			initCloseHud();
		}// else if(event.getTypeId().equals(BulletMissedEvent.TYPE_ID)){
//			missedShots++;
//		} 
		
		
	}

	private void initCloseHud()  {
		
	}
	



}
