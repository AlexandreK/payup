package com.diamonddev.payup.game.hud;

import org.cogaen.box2d.PoseUpdateEvent;
import org.cogaen.core.Core;
import org.cogaen.event.Event;
import org.cogaen.event.EventListener;
import org.cogaen.event.EventService;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.scene.Camera;
import org.cogaen.lwjgl.scene.CircleVisual;
import org.cogaen.lwjgl.scene.Color;
import org.cogaen.lwjgl.scene.RectangleVisual;
import org.cogaen.lwjgl.scene.SceneNode;
import org.cogaen.lwjgl.scene.SceneService;
import org.cogaen.lwjgl.scene.TextVisual;
import org.cogaen.lwjgl.scene.Visual;
import org.cogaen.name.CogaenId;
import org.cogaen.resource.ResourceService;
import org.cogaen.resource.TextHandle;
import org.cogaen.view.AbstractHud;

import com.diamonddev.payup.game.event.CompassChangedEvent;
import com.diamonddev.payup.game.event.GameTimeEvent;
import com.diamonddev.payup.game.event.LevelEvent;
import com.diamonddev.payup.game.event.MapBoundsEvent;

public class GameTimeHud extends AbstractHud implements EventListener {

	private CogaenId resourceGrp;
	
	
	SceneNode node;
	SceneService scene;
	TextVisual time;
	

	public GameTimeHud(Core core) {
		super(core);
		EventService.getInstance(getCore()).addListener(this, GameTimeEvent.TYPE_ID);
		EventService.getInstance(getCore()).addListener(this, PoseUpdateEvent.TYPE_ID);
	}

	@Override
	public void registerResources(CogaenId groupId) {
		if(!ResourceService.getInstance(getCore()).isDeclared("scoreTxt")){
		this.resourceGrp = groupId;
		ResourceService serSrv =ResourceService.getInstance(getCore());
		serSrv.declareResource("scoreTxt", groupId, new TextHandle("scoreTxt"));
		}
	}

	@Override
	public void engage() {
		super.engage();
		scene = SceneService.getInstance(getCore());
		
		node = scene.createNode();
		
		time = new TextVisual(getCore(),"HudFnt","Time:");
		time.setColor(Color.WHITE);
		time.setScale(0.03);
		Camera camera = scene.getCamera(0);
		node.addVisual(time);
		node.setPose(camera.getPosX()+17, camera.getPosY()+5, 0);
		
		
		scene.getLayer(0).addNode(node);
		
	}


	@Override
	public void disengage() {
		super.disengage();
	}

	@Override
	public void handleEvent(Event event) {
		if(event.getTypeId().equals(PoseUpdateEvent.TYPE_ID)){
			Camera camera = scene.getCamera(0);
			node.setPose(camera.getPosX()+17, camera.getPosY()+5, node.getAngle());
		} else if(event.getTypeId().equals(GameTimeEvent.TYPE_ID)){
			GameTimeEvent evt = (GameTimeEvent) event;
			time.setText("Time: " + evt.getTime());
		}
		
		
	}
	



}
