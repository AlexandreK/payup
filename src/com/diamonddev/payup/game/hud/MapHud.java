package com.diamonddev.payup.game.hud;

import org.cogaen.box2d.PoseUpdateEvent;
import org.cogaen.core.Core;
import org.cogaen.event.Event;
import org.cogaen.event.EventListener;
import org.cogaen.event.EventService;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.scene.Camera;
import org.cogaen.lwjgl.scene.CircleVisual;
import org.cogaen.lwjgl.scene.Color;
import org.cogaen.lwjgl.scene.RectangleVisual;
import org.cogaen.lwjgl.scene.SceneNode;
import org.cogaen.lwjgl.scene.SceneService;
import org.cogaen.lwjgl.scene.SpriteVisual;
import org.cogaen.lwjgl.scene.TextVisual;
import org.cogaen.lwjgl.scene.TextureHandle;
import org.cogaen.lwjgl.scene.Visual;
import org.cogaen.name.CogaenId;
import org.cogaen.property.PropertyService;
import org.cogaen.resource.ResourceService;
import org.cogaen.resource.TextHandle;
import org.cogaen.view.AbstractHud;

import com.diamonddev.payup.game.event.CompassChangedEvent;
import com.diamonddev.payup.game.event.LevelEvent;
import com.diamonddev.payup.game.event.MapBoundsEvent;

public class MapHud extends AbstractHud implements EventListener {

	private CogaenId resourceGrp;
	
	
	SceneNode node;
	SceneService scene;
	SpriteVisual map;
	CircleVisual position;
	private SceneNode positionNode;
	

	public MapHud(Core core) {
		super(core);
		EventService.getInstance(getCore()).addListener(this, LevelEvent.TYPE_ID);
		EventService.getInstance(getCore()).addListener(this, PoseUpdateEvent.TYPE_ID);
	}

	@Override
	public void registerResources(CogaenId groupId) {
		if(!ResourceService.getInstance(getCore()).isDeclared("map")){
		this.resourceGrp = groupId;
		ResourceService serSrv =ResourceService.getInstance(getCore());
		String path = PropertyService.getInstance(getCore()).getProperty(
				"mapPath",
				"res/map.gif");
		serSrv.declareResource("map", groupId, new TextureHandle("gif", path));
		}
	}

	@Override
	public void engage() {
		super.engage();
		scene = SceneService.getInstance(getCore());
		
		node = scene.createNode();
		
		map = new SpriteVisual(getCore(),"map",10,10);
		map.setColor(Color.WHITE);
//		map.setScale(0.03);
		map.setFlipVertical(true);
		Camera camera = scene.getCamera(0);
		node.addVisual(map);
		node.setPose(camera.getPosX()-20, camera.getPosY()+15, 0);
		
		position = new CircleVisual(0.2);
		position.setColor(Color.RED);
		
		positionNode = scene.createNode();
		positionNode.addVisual(position);
		positionNode.setPose(camera.getPosX()-20, camera.getPosX()+13, 0);
		
		scene.getLayer(1).addNode(node);
		scene.getLayer(0).addNode(positionNode);
		
	}


	@Override
	public void disengage() {
		super.disengage();
	}

	@Override
	public void handleEvent(Event event) {
		if(event.getTypeId().equals(PoseUpdateEvent.TYPE_ID)){
			Camera camera = scene.getCamera(0);
			node.setPose(camera.getPosX()-20, camera.getPosY()+13, 0);
			positionNode.setPose((camera.getPosX()-20) + (camera.getPosX()/50)-5, (camera.getPosY()+13) + (camera.getPosY()/50)-5, 0);
		} else if(event.getTypeId().equals(LevelEvent.TYPE_ID)){
			LevelEvent evt = (LevelEvent) event;
//			map.setText("Level: " + evt.getLevel());
		}// else if(event.getTypeId().equals(BulletMissedEvent.TYPE_ID)){
//			missedShots++;
//		} 
		
		
	}
	



}
