package com.diamonddev.payup.game.hud;

import org.cogaen.box2d.PoseUpdateEvent;
import org.cogaen.core.Core;
import org.cogaen.event.Event;
import org.cogaen.event.EventListener;
import org.cogaen.event.EventService;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.scene.Camera;
import org.cogaen.lwjgl.scene.CircleVisual;
import org.cogaen.lwjgl.scene.Color;
import org.cogaen.lwjgl.scene.RectangleVisual;
import org.cogaen.lwjgl.scene.SceneNode;
import org.cogaen.lwjgl.scene.SceneService;
import org.cogaen.lwjgl.scene.TextVisual;
import org.cogaen.lwjgl.scene.Visual;
import org.cogaen.name.CogaenId;
import org.cogaen.resource.ResourceService;
import org.cogaen.resource.TextHandle;
import org.cogaen.view.AbstractHud;

import com.diamonddev.payup.game.event.CompassChangedEvent;
import com.diamonddev.payup.game.event.MapBoundsEvent;

public class CompassHud extends AbstractHud implements EventListener {

	private CogaenId resourceGrp;
	private int enemiesKilled;
	private int hitShots;
	private int missedShots;
	
	private static final int pForHits = 1;
	private static final int pForEnemy = 5;
	private static final int pForMiss = 2;
	
	SceneNode needleNode;
	SceneNode goalNode;
	SceneService scene;
	TextVisual distance;
	

	public CompassHud(Core core) {
		super(core);
		EventService.getInstance(getCore()).addListener(this, PoseUpdateEvent.TYPE_ID);
		EventService.getInstance(getCore()).addListener(this, CompassChangedEvent.TYPE_ID);
//		EventService.getInstance(getCore()).addListener(this, BulletHitEvent.TYPE_ID);
	}

	@Override
	public void registerResources(CogaenId groupId) {
		if(!ResourceService.getInstance(getCore()).isDeclared("scoreTxt")){
		this.resourceGrp = groupId;
		ResourceService serSrv =ResourceService.getInstance(getCore());
		serSrv.declareResource("scoreTxt", groupId, new TextHandle("scoreTxt"));
		}
	}

	@Override
	public void engage() {
		super.engage();
		scene = SceneService.getInstance(getCore());
		
		needleNode = scene.createNode();
		goalNode = scene.createNode();
		
		distance = new TextVisual(getCore(),"HudFnt","");
		distance.setColor(Color.RED);
		distance.setScale(0.01);
		Camera camera = scene.getCamera(0);
		CircleVisual background = new CircleVisual(2.0);
		background.setColor(Color.WHITE);
		RectangleVisual needle = new RectangleVisual(3.0,0.5);
		needle.setFilled(true);
		needle.setColor(Color.BLACK);
		needleNode.addVisual(background);
		needleNode.addVisual(needle);
		needleNode.addVisual(distance);
		needleNode.setPose(camera.getPosX()+20, camera.getPosY()+15, 0);
		
		CircleVisual goal = new CircleVisual(1.0);
		goal.setColor(Color.GREEN);
		goalNode.addVisual(goal);
		
		
		
		scene.getLayer(0).addNode(needleNode);
		scene.getLayer(0).addNode(goalNode);
		
	}


	@Override
	public void disengage() {
		super.disengage();
	}

	@Override
	public void handleEvent(Event event) {
		if(event.getTypeId().equals(PoseUpdateEvent.TYPE_ID)){
			Camera camera = scene.getCamera(0);
			needleNode.setPose(camera.getPosX()+20, camera.getPosY()+15, needleNode.getAngle());
		} else if(event.getTypeId().equals(CompassChangedEvent.TYPE_ID)){
			CompassChangedEvent evt = (CompassChangedEvent) event;
			needleNode.setPose(needleNode.getPosX(), needleNode.getPosY(), evt.getAngle());
			goalNode.setPose(evt.getGoal().x, evt.getGoal().y, 0);
			distance.setText(evt.getDistance() + "meters left!");
		}// else if(event.getTypeId().equals(BulletMissedEvent.TYPE_ID)){
//			missedShots++;
//		} 
		
		updateHud();
		
	}
	
	private void updateHud() {
//		((TextVisual)scoreVisual).setText(getScoreText());
//		missed.setText(getMissedText());
//		hit.setText(getHitText());		
	}



}
