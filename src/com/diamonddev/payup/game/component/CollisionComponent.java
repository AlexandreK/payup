package com.diamonddev.payup.game.component;

import org.cogaen.box2d.Pose2D;
import org.cogaen.entity.ComponentEntity;
import org.cogaen.entity.EntityService;
import org.cogaen.entity.UpdateableComponent;
import org.cogaen.event.Event;
import org.cogaen.event.EventListener;
import org.cogaen.event.EventService;
import org.cogaen.logging.LoggingService;
import org.cogaen.name.CogaenId;

import com.diamonddev.payup.game.entity.Collidable;
import com.diamonddev.payup.game.event.CollisionListener;
import com.diamonddev.payup.game.event.PrepareToDieEvent;
import com.diamonddev.payup.game.service.CollisionService;
import com.diamonddev.payup.game.util.BoundingBox;


public class CollisionComponent extends UpdateableComponent implements Collidable, EventListener {
	private boolean isActive = false;
	private boolean alive = true;
	
	public CollisionComponent(boolean active){
		isActive = active;
	}
	public void initialize(ComponentEntity parent){
		super.initialize(parent);
		
		if(isActive){
			CollisionService.getInstance(getCore()).appendActiveCollidable(this, getParent().getType());	
		} else{
			CollisionService.getInstance(getCore()).appendPassiveCollidable(this, getParent().getType());
		}
		EventService.getInstance(getCore()).addListener(this, PrepareToDieEvent.TYPE_ID);
	}
	
	@Override
	public void update() {
	}

	@Override
	public BoundingBox getBoundingBox() {
		CollisionListener entity = (CollisionListener)getParent();
		return entity.getBoundingBox();
	}
	
	private double getPosX(){
		Pose2D position = (Pose2D)getParent().getAttribute(Pose2D.ATTR_ID);
		return position.getPosX();
	}
	private double getPosY() {
		Pose2D position = (Pose2D)getParent().getAttribute(Pose2D.ATTR_ID);
		return position.getPosY();
	}

	@Override
	public void handleCollision(Collidable c) {
		CollisionListener entity = (CollisionListener)getParent();
		entity.handleCollision(c);
	}
	
	@Override
	public void handleEvent(Event event) {
		if(event.getTypeId().equals(PrepareToDieEvent.TYPE_ID)){
			PrepareToDieEvent e = (PrepareToDieEvent)event;
			if(e.getDieId().equals(getParent().getId())){
				if(isActive){
					CollisionService.getInstance(getCore()).removeActiveCollidable(this);	
				} else{
					CollisionService.getInstance(getCore()).removePassiveCollidable(this);
				}
			}
		}
	}
}
