package com.diamonddev.payup.game.component;


import org.cogaen.box2d.Pose2D;
import org.cogaen.entity.Component;
import org.cogaen.event.EventService;

import com.diamonddev.payup.game.event.DestroyEvent;
import com.diamonddev.payup.game.event.SpawnEvent;

public class LifecycleComponent extends Component {

	@Override
	public void engage() {
		super.engage();
		
		Pose2D pose = (Pose2D) getParent().getAttribute(Pose2D.ATTR_ID);
		SpawnEvent event = new SpawnEvent(getParent().getId(), getParent().getType(), pose);
		EventService.getInstance(getCore()).dispatchEvent(event);
	}

	@Override
	public void disengage() {
		Pose2D pose = (Pose2D) getParent().getAttribute(Pose2D.ATTR_ID);
		DestroyEvent event = new DestroyEvent(getParent().getId(), getParent().getType(), pose);
		EventService.getInstance(getCore()).dispatchEvent(event);
		
		super.disengage();
	}

}
