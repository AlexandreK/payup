package com.diamonddev.payup.game.component;

import org.cogaen.box2d.Pose2D;
import org.cogaen.entity.Component;
import org.cogaen.entity.ComponentEntity;


public class StaticPoseComponent extends Component implements Pose2D {

	private double x;
	private double y;
	private double angle;
	
	@Override
	public void initialize(ComponentEntity parent) {
		super.initialize(parent);
		
		getParent().addAttribute(Pose2D.ATTR_ID, this);
	}

	@Override
	public double getPosX() {
		return this.x;
	}

	@Override
	public double getPosY() {
		return this.y;
	}

	@Override
	public double getAngle() {
		return this.angle;
	}

	@Override
	public void setPosition(double x, double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public void setAngle(double angle) {
		this.angle = angle;
	}

}
