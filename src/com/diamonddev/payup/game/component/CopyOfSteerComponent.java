//package com.diamonddev.payup.game.component;
//
//import org.cogaen.entity.UpdateableComponent;
//import org.cogaen.event.Event;
//import org.cogaen.event.EventListener;
//import org.cogaen.event.EventService;
//import org.cogaen.logging.LoggingService;
//import org.cogaen.lwjgl.input.ControllerState;
//import org.cogaen.lwjgl.scene.Camera;
//import org.cogaen.lwjgl.scene.SceneService;
//import org.cogaen.math.Vector2;
//import org.cogaen.time.TimeService;
//import org.cogaen.time.Timer;
//
//import com.diamonddev.payup.game.PlayState;
//import com.diamonddev.payup.game.entity.CarEntity;
//import com.diamonddev.payup.game.entity.HumanEntity;
//import com.diamonddev.payup.game.entity.Pose2D;
//import com.diamonddev.payup.game.event.CollisionEvent;
//import com.diamonddev.payup.game.event.EnterCarEvent;
//import com.diamonddev.payup.game.event.ExitCarEvent;
//import com.diamonddev.payup.game.event.MapBoundsEvent;
//import com.diamonddev.payup.game.event.PoseUpdateEvent;
//import com.diamonddev.payup.game.event.StartRunningEvent;
//import com.diamonddev.payup.game.event.StopRunningEvent;
//import com.diamonddev.payup.game.util.Collision;
//import com.diamonddev.payup.game.util.Log;
//
//public class CopyOfSteerComponent extends UpdateableComponent implements EventListener {
//
//
//	private static final double RANGE = 0.75;
//	private static final double SPEED_RUNNING = HumanEntity.getSpeed(true);
//	private static final double SPEED_WALKING = HumanEntity.getSpeed(false);
//	
//	private Pose2D pose;
//	private ControllerState ctrlState;
//	private Timer timer;
//	private EventService evtSrv;
//	private boolean isRunning = false;
//	private boolean isDriving = false;
//	private Collision collision = new Collision();
//
//	@Override
//	public void engage() {
//		super.engage();
//		this.pose = (Pose2D) getParent().getAttribute(Pose2D.ATTR_ID);
//		this.ctrlState = (ControllerState) getParent().getAttribute(ControllerState.ID);
//		this.timer = TimeService.getInstance(getCore()).getTimer();
//		this.evtSrv = EventService.getInstance(getCore());
//		this.evtSrv.addListener(this, StartRunningEvent.TYPE_ID);
//		this.evtSrv.addListener(this, StopRunningEvent.TYPE_ID);
//		this.evtSrv.addListener(this, EnterCarEvent.TYPE_ID);
//		this.evtSrv.addListener(this, ExitCarEvent.TYPE_ID);
//		this.evtSrv.addListener(this, CollisionEvent.TYPE_ID);
//		
//	}
//
//	@Override
//	public void update() {
////		LoggingService.getInstance(getCore()).logDebug("STEER", this.ctrlState.getHorizontalPosition() + "");
//		double x = this.pose.getPosX() + getSpeed() * this.timer.getDeltaTime() * getHorizontalPosition();
//		double y = this.pose.getPosY() + getSpeed() * this.timer.getDeltaTime() * getVerticalPosition();
//		
//		double oldX = this.pose.getPosX();
//		double oldY = this.pose.getPosY();
//		
//		
//		SceneService scnSrv = SceneService.getInstance(getCore());
//		Camera camera = scnSrv.getCamera(0);
//		
//		if (x < camera.getPosX() + (-PlayState.DISPLAY_WIDTH * RANGE / 2)) {
//			evtSrv.dispatchEvent(new MapBoundsEvent(-1,0));
//		} else if (x > camera.getPosX() + (PlayState.DISPLAY_WIDTH * RANGE / 2)) {
//			evtSrv.dispatchEvent(new MapBoundsEvent(1,0));
//		}
//		if (y < camera.getPosY() + (-PlayState.DISPLAY_WIDTH/scnSrv.getAspectRatio() * RANGE / 2)) {
//			evtSrv.dispatchEvent(new MapBoundsEvent(0,-1));
//		} else if (y > camera.getPosY() +(PlayState.DISPLAY_WIDTH/scnSrv.getAspectRatio() * RANGE / 2)) {
//			evtSrv.dispatchEvent(new MapBoundsEvent(0,1));
//		}
//		
//		Vector2 oldPoint = new Vector2(oldX,oldY); 
//		Vector2 newPoint= new Vector2(x,y);
//		Vector2 reference = new Vector2(0,1);
//		
//		if(oldX<x){
//			reference.set(0, -1);
//		}
//		Vector2 line = new Vector2(0,1);
//		line.sub(newPoint, oldPoint);
//		
//		double angle = (double)Math.acos(reference.dot(line)/line.length()*reference.length());
//		if(!(angle>=0)){
//			angle=this.pose.getAngle();
//		}
//		
//		this.pose.setPosition(x, y);
//		if(oldX<x){
//			angle = angle-Math.PI;
//		}
//		this.pose.setAngle(angle);
//		this.evtSrv.dispatchEvent(new PoseUpdateEvent(getParent().getId(), pose));
//		collision.reset();
//		
//	}
//
//	private double getHorizontalPosition() {
//		double x = this.ctrlState.getHorizontalPosition();
//		if(collision.isCollideEast() && x>0){
//			x=0;
//		}
//		if(collision.isCollideWest() && x<0){
//			x=0;
//		}
//		
//		return x;
//	}
//	private double getVerticalPosition() {
//		double y = this.ctrlState.getVerticalPosition();
//		if(collision.isCollideNorth() && y>0){
//			y=0;
//		}
//		if(collision.isCollideSouth() && y<0){
//			y=0;
//		}
//		
//		return y;
//	}
//
//	private double getSpeed() {
//		if(isRunning){
//			return SPEED_RUNNING;
//		}else{
//			return SPEED_WALKING;
//		}
//	}
//	
//	@Override
//	public void handleEvent(Event event) {
//		if(event.isOfType(StartRunningEvent.TYPE_ID)){
//			isRunning = true;
//		} else if(event.isOfType(StopRunningEvent.TYPE_ID)){
//			isRunning = false;
//		} else if(event.isOfType(CollisionEvent.TYPE_ID)){
//			if( ((CollisionEvent) event).getEntityId().equals(getParent().getId())){
//				collision = ((CollisionEvent)event).getCollision();
//			}
//		}
//	}
//
//
//}
