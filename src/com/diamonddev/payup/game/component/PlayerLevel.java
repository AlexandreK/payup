package com.diamonddev.payup.game.component;

import org.cogaen.name.CogaenId;

public interface PlayerLevel {
	
	public static final CogaenId ATTR_ID = new CogaenId("PlayerLevel");
	
	public int getLevel();

}
