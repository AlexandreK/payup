package com.diamonddev.payup.game.component;

import java.awt.Point;
import java.util.List;
import java.util.Random;

import org.cogaen.box2d.Pose2D;
import org.cogaen.box2d.PoseUpdateEvent;
import org.cogaen.entity.ComponentEntity;
import org.cogaen.entity.UpdateableComponent;
import org.cogaen.event.Event;
import org.cogaen.event.EventListener;
import org.cogaen.event.EventService;
import org.cogaen.math.Vector2;
import org.cogaen.time.TimeService;
import org.cogaen.time.Timer;

import com.diamonddev.payup.game.PlayState;
import com.diamonddev.payup.game.entity.Moveable;
import com.diamonddev.payup.game.event.CollisionEvent;
import com.diamonddev.payup.game.service.MapService;
import com.diamonddev.payup.game.util.Collision;
import com.diamonddev.payup.game.util.Log;

public class KIComponent extends UpdateableComponent implements EventListener,
		Moveable {

	private Pose2D pose;
	private Vector2 position = new Vector2(0, 0);
	private Vector2 velocity = new Vector2(0, 1);
	private double angle;
	private double speed = 5.0;
	private Random rdm = new Random();
	private Timer timer;
	private EventService evtSrv;
	private List<Vector2> path;
	private static double updateTime = 0.2;
	private double nextUpdate = 0;
	private int nextStep = 0;
	private Vector2 nextPoint;
	int counter = 0;

	private Collision collision = new Collision();

	@Override
	public void update() {
		// if(nextUpdate<timer.getTime()){

		Vector2 temp = new Vector2();
		temp.sub(position, nextPoint);

		if (temp.length() < 0.1) {
			if (nextStep >= path.size()) {
				setGoal();
			}
			position = nextPoint;
			nextPoint = path.get(nextStep++);
		}

		Vector2 richtung = new Vector2(nextPoint.x, nextPoint.y);
		richtung.sub(position);
		richtung.normalize();
		// richtung.scale(speed*timer.getDeltaTime());
		// Log.log(richtung.x+""+richtung.y);

		Vector2 step = position;
		step.scaleAdd(this.speed * this.timer.getDeltaTime(), richtung,
				this.position);

		//TODO: SET ANGLE
//		Vector2 reference = new Vector2(0,1);
//		
//		if(position.x<step.x){
//			reference.set(0, -1);
//		}
//		Vector2 line = new Vector2(0,1);
//		line.sub(step, position);
//		
//		double angle = (double)Math.acos(reference.dot(line)/line.length()*reference.length());
//		if(!(angle>=0)){
//			angle=this.pose.getAngle();
//		}
//		
//		if(position.x<step.x){
//			angle = angle-Math.PI;
//		}
//		this.pose.setAngle(angle);
		pose.setPosition(step.x-3, step.y-3);
		this.evtSrv
				.dispatchEvent(new PoseUpdateEvent(getParent().getId(), pose));

		// }
		//
		// if(nextUpdate<timer.getTime()){
		// setNextUpdate();
		// if(nextStep>=path.size()){
		// setGoal();
		// }
		// pose.setPosition(nextPoint.x, nextPoint.y);
		// nextPoint = path.get(nextStep++);
		// this.evtSrv.dispatchEvent(new PoseUpdateEvent(getParent().getId(),
		// pose));
		// }
	}

	private void setNextUpdate() {
		nextUpdate = timer.getTime() + updateTime;
	}

	@Override
	public void initialize(ComponentEntity parent) {
		super.initialize(parent);
		this.pose = (Pose2D) getParent().getAttribute(Pose2D.ATTR_ID);
		this.timer = TimeService.getInstance(getCore()).getTimer();
		this.evtSrv = EventService.getInstance(getCore());
		this.evtSrv.addListener(this, CollisionEvent.TYPE_ID);
		position = MapService.getInstance(getCore()).getSpawnPoint();
		this.pose.setPosition(position.x, position.y);

		setGoal();
		setNextUpdate();
	}

	private void setGoal() {

		path = null;

		while (path == null || path.size()<1) {
			Vector2 start = new Vector2((int) pose.getPosX(),
					(int) pose.getPosY());
			Vector2 goal = new Vector2(rdm.nextInt(PlayState.WORLD_WIDTH),
					rdm.nextInt(PlayState.WORLD_WIDTH));
			path = MapService.getInstance(getCore()).getPath(start, goal);
		}
		nextPoint = path.get(0);
		nextStep = 1;
	}

	@Override
	public void handleEvent(Event event) {
	}

	@Override
	public void setAngle(double angle) {
		this.angle = angle;

	}

	@Override
	public void setSpeed(double speed) {
		this.speed = speed;

	}

}
