package com.diamonddev.payup.game.component;

import org.cogaen.entity.ComponentEntity;
import org.cogaen.entity.EntityService;
import org.cogaen.entity.UpdateableComponent;
import org.cogaen.time.TimeService;
import org.cogaen.time.Timer;

public class TimeToLiveComponent extends UpdateableComponent {

	public double ttl;
	public double timeStamp;
	public Timer timer;
	
	public TimeToLiveComponent(double ttl) {
		this.ttl = ttl;
	}

	@Override
	public void initialize(ComponentEntity parent) {
		super.initialize(parent);
		this.timer = TimeService.getInstance(getCore()).getTimer();
	}

	@Override
	public void engage() {
		super.engage();
		this.timeStamp = this.timer.getTime() + this.ttl;
	}

	@Override
	public void update() {
		if (this.timeStamp <= this.timer.getTime()) {
			EntityService.getInstance(getCore()).removeEntity(getParent().getId());
		}
	}

}
