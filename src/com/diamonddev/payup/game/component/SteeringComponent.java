package com.diamonddev.payup.game.component;

import org.cogaen.box2d.PhysicsBody;
import org.cogaen.box2d.Pose2D;
import org.cogaen.box2d.PoseUpdateEvent;
import org.cogaen.entity.UpdateableComponent;
import org.cogaen.event.EventService;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.input.ControllerState;
import org.cogaen.lwjgl.scene.Camera;
import org.cogaen.lwjgl.scene.SceneService;

import com.diamonddev.payup.game.PlayState;
import com.diamonddev.payup.game.event.MapBoundsEvent;
import com.diamonddev.payup.game.event.ThrustEvent;

public class SteeringComponent extends UpdateableComponent {

	private static final double THRUST_FORCE = 100;
	private static final double TORQUE = 10;
	private static final double RANGE = 0.3;
	private ControllerState ctrlState;
	private PhysicsBody body;
	private double oldThrust = 0;
	
	@Override
	public void engage() {
		super.engage();
		this.ctrlState = (ControllerState) getParent().getAttribute(ControllerState.ID);
		this.body = (PhysicsBody) getParent().getAttribute(PhysicsBody.PHYSICS_BODY_ATTRIB);
	}

	@Override
	public void update() {
		
//		double verticalForce = THRUST_FORCE * this.ctrlState.getVerticalPosition();
//		double horizontalForce = this.ctrlState.getHorizontalPosition();
////		if(verticalForce < 0){
////			verticalForce *= -1;
////			horizontalForce *= -1;
////		}
//		
//		this.body.applyRelativeForce(0, verticalForce, 0, 0);
//		this.body.applyTorque(-TORQUE * horizontalForce);
//		
//		
//		if (verticalForce != this.oldThrust) {
//			EventService.getInstance(getCore()).dispatchEvent(new ThrustEvent(getParent().getId(), verticalForce));
//			this.oldThrust = verticalForce;
//		}
		SceneService scnSrv = SceneService.getInstance(getCore());
		EventService evtSrv = EventService.getInstance(getCore());
		Camera camera = scnSrv.getCamera(0);
		Pose2D pose = (Pose2D)getParent().getAttribute(Pose2D.ATTR_ID);
		
		
		
		double x = pose.getPosX();
		double y = pose.getPosY();
//		evtSrv.dispatchEvent(new PoseUpdateEvent(getParent().getId(),pose));
//		if (x < camera.getPosX() + (-PlayState.DISPLAY_WIDTH * RANGE / 2)) {
//			evtSrv.dispatchEvent(new MapBoundsEvent(-1,0));
//		} else if (x > camera.getPosX() + (PlayState.DISPLAY_WIDTH * RANGE / 2)) {
//			evtSrv.dispatchEvent(new MapBoundsEvent(1,0));
//		}
//		if (y < camera.getPosY() + (-PlayState.DISPLAY_WIDTH/scnSrv.getAspectRatio() * RANGE / 2)) {
//			evtSrv.dispatchEvent(new MapBoundsEvent(0,-1));
//		} else if (y > camera.getPosY() +(PlayState.DISPLAY_WIDTH/scnSrv.getAspectRatio() * RANGE / 2)) {
//			evtSrv.dispatchEvent(new MapBoundsEvent(0,1));
//		}
	}

}
