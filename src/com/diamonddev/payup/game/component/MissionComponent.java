package com.diamonddev.payup.game.component;

import java.util.Random;

import org.cogaen.box2d.Pose2D;
import org.cogaen.entity.ComponentEntity;
import org.cogaen.entity.UpdateableComponent;
import org.cogaen.event.EventService;
import org.cogaen.event.SimpleEvent;
import org.cogaen.logging.LoggingService;
import org.cogaen.math.Vector2;

import com.diamonddev.payup.game.PlayState;
import com.diamonddev.payup.game.event.CompassChangedEvent;
import com.diamonddev.payup.game.event.LevelEvent;
import com.diamonddev.payup.game.event.MoneyEarnedEvent;
import com.diamonddev.payup.game.logic.Level;
import com.diamonddev.payup.game.service.GameTimeService;
import com.diamonddev.payup.game.service.LevelService;
import com.diamonddev.payup.game.service.MapService;

public class MissionComponent extends UpdateableComponent implements PlayerLevel {

	private static final double RANGE = 2.5;
	private static final int MISSIONS_PER_LEVEL = 2;
	private Vector2 goal;
	private double angle;
	double distance;
	private double reward;
	private int debts = 50000;
	private int accomplishedMissions;
	private int level = 1; 

	public MissionComponent() {
	}
	
	@Override
	public void initialize(ComponentEntity parent) {
		super.initialize(parent);
		
		getParent().addAttribute(PlayerLevel.ATTR_ID, this);
		checkLevel();
		EventService.getInstance(getCore()).dispatchEvent(new MoneyEarnedEvent(debts));
		
	}

	@Override
	public void update() {
		if(goal==null)createGoal();
		Pose2D pose = (Pose2D) getParent().getAttribute(Pose2D.ATTR_ID);

		if (!goalReached(new Vector2(pose.getPosX(), pose.getPosY()))) {
			EventService.getInstance(getCore()).dispatchEvent(new CompassChangedEvent(angle,goal,(int)distance));
		} else {
			accomplishedMissions++;
//			checkLevel();
			reduceDebts(reward);
			
			createGoal();
		}

	}

	private void reduceDebts(double reward2) {
		debts -= reward2;
		if(debts <= 0){
			EventService.getInstance(getCore()).dispatchEvent(new SimpleEvent(PlayState.DEBTS_REPAYED));
		}
		EventService.getInstance(getCore()).dispatchEvent(new MoneyEarnedEvent(debts));
			
		
	}

	private void checkLevel() {
//		int newLevel = ((accomplishedMissions/MISSIONS_PER_LEVEL)+1)%10 +1 ;
//		if(mission < newLevel){
//			
//			mission = newLevel;
//			GameTimeService.getInstance(getCore()).incrementTime();
//		}
		
	}

	private boolean goalReached(Vector2 pos) {

		Vector2 dir = new Vector2(pos);
		dir.sub(goal);
		distance = dir.length();
		angle = Math.atan2(goal.y-pos.y, goal.x-pos.x);
		return distance <= RANGE;
	}
	
	private void createGoal(){
		Level l = LevelService.getInstance(getCore()).getLevel();
		goal = l.getPosition();
		reward = l.getReward();
		EventService.getInstance(getCore()).dispatchEvent(new LevelEvent(l));
//		Pose2D pose = (Pose2D) getParent().getAttribute(Pose2D.ATTR_ID);
//		Vector2 startposition = new Vector2(pose.getPosX(), pose.getPosY());
//		goal = MapService.getInstance(getCore()).getMissionGoal(startposition, getLevel());
//		startposition.sub(goal);
//		reward = startposition.length(); 
	}


	@Override
	public int getLevel() {
		return level ;
	}

	public int getDebts() {
		return debts;
	}

	public void setDebts(int debts) {
		this.debts = debts;
	}

}
