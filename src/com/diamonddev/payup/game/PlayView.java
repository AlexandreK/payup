package com.diamonddev.payup.game;

import org.cogaen.box2d.PoseUpdateEvent;
import org.cogaen.core.Core;
import org.cogaen.event.Event;
import org.cogaen.event.EventListener;
import org.cogaen.event.EventService;
import org.cogaen.logging.LoggingService;
import org.cogaen.lwjgl.input.KeyCode;
import org.cogaen.lwjgl.input.KeyPressedEvent;
import org.cogaen.lwjgl.input.KeyReleasedEvent;
import org.cogaen.lwjgl.input.KeyboardController;
import org.cogaen.lwjgl.scene.Camera;
import org.cogaen.lwjgl.scene.Color;
import org.cogaen.lwjgl.scene.FontHandle;
import org.cogaen.lwjgl.scene.SceneService;
import org.cogaen.lwjgl.scene.SpriteHandle;
import org.cogaen.lwjgl.scene.TextureHandle;
import org.cogaen.name.CogaenId;
import org.cogaen.resource.ResourceService;
import org.cogaen.view.View;

import com.diamonddev.payup.game.entity.BuildingEntity;
import com.diamonddev.payup.game.entity.CarEntity;
import com.diamonddev.payup.game.entity.PlayerEntity;
import com.diamonddev.payup.game.entity.humans.PedestrianEntity;
import com.diamonddev.payup.game.entity.textures.QuayEntity;
import com.diamonddev.payup.game.entity.textures.RoadEntity;
import com.diamonddev.payup.game.entity.textures.SandEntity;
import com.diamonddev.payup.game.entity.textures.SidewalkEntity;
import com.diamonddev.payup.game.entity.textures.TextureEntity;
import com.diamonddev.payup.game.entity.textures.WaterEntity;
import com.diamonddev.payup.game.event.DestroyEvent;
import com.diamonddev.payup.game.event.InteractWithCarEvent;
import com.diamonddev.payup.game.event.MapBoundsEvent;
import com.diamonddev.payup.game.event.SpawnEvent;
import com.diamonddev.payup.game.event.StartBrakingEvent;
import com.diamonddev.payup.game.event.StartRunningEvent;
import com.diamonddev.payup.game.event.StopBrakingEvent;
import com.diamonddev.payup.game.event.StopRunningEvent;
import com.diamonddev.payup.game.representation.BaseRepresentation;
import com.diamonddev.payup.game.representation.BuildingRepresentation;
import com.diamonddev.payup.game.representation.CarRepresentation;
import com.diamonddev.payup.game.representation.HumanRepresentation;
import com.diamonddev.payup.game.representation.PlayerRepresentation;
import com.diamonddev.payup.game.representation.QuayRepresentation;
import com.diamonddev.payup.game.representation.RoadRepresentation;
import com.diamonddev.payup.game.representation.TextureRepresentation;
import com.diamonddev.payup.game.representation.SidewalkRepresentation;
import com.diamonddev.payup.game.representation.WaterRepresentation;
import com.diamonddev.payup.game.service.MapService;
import com.diamonddev.payup.game.util.Log;

public class PlayView extends View implements EventListener {

	KeyboardController ctrl;
	private Camera camera;

	public PlayView(Core core) {
		super(core);
		this.ctrl = new KeyboardController(getCore(), PlayState.PLAYER_ENTITY_ID);
		this.ctrl.setAxisKeys(KeyCode.KEY_A, KeyCode.KEY_D, KeyCode.KEY_W, KeyCode.KEY_S);
//		this.ctrl.addButton(KeyCode.KEY_DOWN);
//		this.ctrl.addButton(KeyCode.KEY_UP);
	}
	
	@Override
	public void registerResources(CogaenId groupId) {
		super.registerResources(groupId);
		
		ResourceService resSrv = ResourceService.getInstance(getCore());
//		resSrv.getResource(name)
		resSrv.declareResource("RoadFt", groupId, new FontHandle("Verdana", 50));
		resSrv.declareResource("HudFnt", groupId, new FontHandle("SansSerif", 50));
		resSrv.declareResource("building_1", groupId, new TextureHandle("png","res/buildings/building_1.png"));
		resSrv.declareResource("building_2", groupId, new TextureHandle("png","res/buildings/building_2.png"));
		resSrv.declareResource("building_3", groupId, new TextureHandle("png","res/buildings/building_3.png"));
		resSrv.declareResource("building_4", groupId, new TextureHandle("png","res/buildings/building_4.png"));
		resSrv.declareResource("hospital", groupId, new TextureHandle("png","res/buildings/building_hospital.png"));
		resSrv.declareResource("police", groupId, new TextureHandle("png","res/buildings/building_police.png"));
		resSrv.declareResource("car", groupId, new TextureHandle("png","res/car.png"));
		resSrv.declareResource("pavement", groupId, new TextureHandle("png","res/sidewalk.png"));
		resSrv.declareResource("water", groupId, new TextureHandle("png","res/water.png"));
		resSrv.declareResource("sand", groupId, new TextureHandle("png","res/sand.png"));
		resSrv.declareResource("road", groupId, new TextureHandle("png","res/road.png"));
		resSrv.declareResource("quay", groupId, new TextureHandle("png","res/quaywall.png"));
		resSrv.declareResource("grass", groupId, new TextureHandle("png","res/grass.png"));
	}

	@Override
	public void engage() {
		super.engage();
		
		EventService evtSrv = EventService.getInstance(getCore());
		evtSrv.addListener(this, SpawnEvent.TYPE_ID);
		evtSrv.addListener(this, DestroyEvent.TYPE_ID);
		evtSrv.addListener(this, PoseUpdateEvent.TYPE_ID);
		evtSrv.addListener(this, MapBoundsEvent.TYPE_ID);
		evtSrv.addListener(this, KeyReleasedEvent.TYPE_ID);
		evtSrv.addListener(this, KeyPressedEvent.TYPE_ID);
//		evtSrv.addListener(this, StartRunningEvent.TYPE_ID);
//		evtSrv.addListener(this, StopRunningEvent.TYPE_ID);

		SceneService scnSrv = SceneService.getInstance(getCore());
		scnSrv.ensuerNumOfLayers(4);
		camera = scnSrv.createCamera();
		camera.setZoom(scnSrv.getScreenWidth() / PlayState.DISPLAY_WIDTH);
		camera.setPosition(PlayState.WORLD_WIDTH/2, PlayState.WORLD_HEIGHT/2);
		MapService.getInstance(getCore()).setActiveCamera(camera);
		scnSrv.setBackgroundColor(new Color(0.7,0.7,0.7));
		this.ctrl.engage();

	}

	@Override
	public void disengage() {
		super.disengage();
	}

	@Override
	public void handleEvent(Event event) {
		if (event.isOfType(SpawnEvent.TYPE_ID)) {
			handleSpawn((SpawnEvent) event);
		} else if (event.isOfType(DestroyEvent.TYPE_ID)) {
			handleDestroy((DestroyEvent) event);
		} else if (event.isOfType(PoseUpdateEvent.TYPE_ID)) {
			handlePoseUpdate((PoseUpdateEvent) event);
		} else if(event.isOfType(MapBoundsEvent.TYPE_ID)){
			handleMapBoundsEvent((MapBoundsEvent)event);
		} else if(event.isOfType(KeyReleasedEvent.TYPE_ID)){
			handleKeyReleased((KeyReleasedEvent)event);
		}else if(event.isOfType(KeyPressedEvent.TYPE_ID)){
			handleKeyPressed((KeyPressedEvent)event);
//		} else if(event.isOfType(StartRunningEvent.TYPE_ID)){
//			setCameraZoom(-2);
//		} else if(event.isOfType(StopRunningEvent.TYPE_ID)){
//			setCameraZoom(2);
		}
	}

	private void handleKeyPressed(KeyPressedEvent event) {
		switch(event.getKeyCode()){
		case KeyCode.KEY_LSHIFT:
			EventService.getInstance(getCore()).dispatchEvent(new StartRunningEvent());
			break;
		case KeyCode.KEY_SPACE:
			EventService.getInstance(getCore()).dispatchEvent(new StartBrakingEvent());
		}
		
	}

	private void handleKeyReleased(KeyReleasedEvent event) {
		switch(event.getKeyCode()){
		case KeyCode.KEY_UP:
			setCameraZoom(1);
			break;
		case KeyCode.KEY_DOWN:
			setCameraZoom(-1);
			break;
		case KeyCode.KEY_LSHIFT:
			EventService.getInstance(getCore()).dispatchEvent(new StopRunningEvent());
			break;
		case KeyCode.KEY_E:
			EventService.getInstance(getCore()).dispatchEvent(new InteractWithCarEvent());
		case KeyCode.KEY_SPACE:
			EventService.getInstance(getCore()).dispatchEvent(new StopBrakingEvent());
		}
		
	}

	private void setCameraZoom(double i) {
		double zoom = camera.getZoom();
		zoom = zoom>1?zoom:2;
		zoom = zoom<50?zoom:49;
//		LoggingService.getInstance(getCore()).logDebug("ZOOM", zoom + "");
		camera.setZoom(zoom+i);
		
	}

	private void handleMapBoundsEvent(MapBoundsEvent event) {
//		camera.setPosition(camera.getPosX()+(camera.getZoom()/PlayState.DISPLAY_WIDTH*event.getX()), camera.getPosY()+(camera.getZoom()/PlayState.DISPLAY_WIDTH*event.getY()));
	}

	private void handlePoseUpdate(PoseUpdateEvent event) {
		if(this.isEngaged()){
		BaseRepresentation er = (BaseRepresentation) getRepresentation(event
				.getEntityId());
		er.setPose(event.getPosX(), event.getPosY(), event.getAngle());
		if(event.getEntityId().equals(PlayState.PLAYER_ENTITY_ID)){
			camera.setPosition(event.getPosX(), event.getPosY());
		}
		}
	}

	private void handleDestroy(DestroyEvent event) {
		if(hasRepresentation(event.getEntityId())){
		removeRepresentation(event.getEntityId());
		} else {
		}
	}

	@SuppressWarnings("deprecation")
	private void handleSpawn(SpawnEvent event) {
//		LoggingService.getInstance(getCore()).logDebug("SPAWN", "spawned");
		if (event.getEntityTypeId().equals(PlayerEntity.TYPE_ID)) {
			BaseRepresentation er = new PlayerRepresentation(getCore(),
					event.getEntityId());
			addRepresentation( er);
			er.setPose(event.getPosX(), event.getPosY(), event.getAngle());
		}else if (event.getEntityTypeId().equals(PedestrianEntity.TYPE_ID)) {
			BaseRepresentation er = new HumanRepresentation(getCore(),
					event.getEntityId());
			addRepresentation(er);
			er.setPose(event.getPosX(), event.getPosY(), event.getAngle());
		}else if (event.getEntityTypeId().equals(BuildingEntity.TYPE_ID)){
			BaseRepresentation er = new BuildingRepresentation(getCore(),
					event.getEntityId());
			addRepresentation(er);
			er.setPose(event.getPosX(), event.getPosY(), event.getAngle());
		} else if(event.getEntityId().equals(CarEntity.TYPE_ID)){
			BaseRepresentation er = new CarRepresentation(getCore(),
					event.getEntityId());
			addRepresentation(er);
			er.setPose(event.getPosX(), event.getPosY(), event.getAngle());
		}else if(event.getEntityTypeId().equals(RoadEntity.TYPE_ID)){
			BaseRepresentation er = new RoadRepresentation(getCore(),
					event.getEntityId());
			addRepresentation(er);
			er.setPose(event.getPosX(), event.getPosY(), event.getAngle());
		}else if(event.getEntityTypeId().equals(SidewalkEntity.TYPE_ID)){
			BaseRepresentation er = new SidewalkRepresentation(getCore(),
					event.getEntityId());
			addRepresentation(er);
			er.setPose(event.getPosX(), event.getPosY(), event.getAngle());
		}else if(event.getEntityTypeId().equals(TextureEntity.TYPE_ID)){
			BaseRepresentation er = new TextureRepresentation(getCore(),
					event.getEntityId());
			addRepresentation(er);
			er.setPose(event.getPosX(), event.getPosY(), event.getAngle());
		}else if(event.getEntityTypeId().equals(SandEntity.TYPE_ID)){
			BaseRepresentation er = new TextureRepresentation(getCore(),
					event.getEntityId());
			addRepresentation(er);
			er.setPose(event.getPosX(), event.getPosY(), event.getAngle());
		}else if(event.getEntityTypeId().equals(QuayEntity.TYPE_ID)){
			BaseRepresentation er = new QuayRepresentation(getCore(),
					event.getEntityId());
			addRepresentation(er);
			er.setPose(event.getPosX(), event.getPosY(), event.getAngle());
		} else {
			LoggingService.getInstance(getCore()).logDebug("REP","Unknown Representation for" + event.getEntityTypeId());
		}
	}

}
